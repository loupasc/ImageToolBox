package net.fraktales.param;

public enum KernelShape {
	SQUARE, DISC, DIAMOND
}
