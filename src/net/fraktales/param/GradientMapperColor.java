package net.fraktales.param;


public enum GradientMapperColor {
	K(0x00, 0x00, 0x00),
	B(0x00, 0x00, 0xAA),
	G(0x00, 0xFF, 0x00),
	C(0x00, 0xFF, 0xFF),
	J(0x55, 0xAA, 0x55),
	R(0xFF, 0x00, 0x00),
	M(0xFF, 0x00, 0xFF),
	P(0xFF, 0x55, 0xAA),
	A(0xFF, 0xAA, 0x00),
	Y(0xFF, 0xFF, 0x00),
	W(0xFF, 0xFF, 0xFF);

	public final int r;
	public final int g;
	public final int b;

	private GradientMapperColor(int r, int g, int b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public static GradientMapperColor getColorFrom(char colorName) {
		for (GradientMapperColor c : GradientMapperColor.values()) {
			if (c.name().charAt(0) == colorName) {
				return c;
			}
		}
		throw new IllegalArgumentException("Unknown color: " + colorName);
	}
}
