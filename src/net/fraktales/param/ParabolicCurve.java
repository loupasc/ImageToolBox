package net.fraktales.param;


public final class ParabolicCurve {
	public static final ParabolicCurve DEFAULT = new ParabolicCurve();
	// action flag
	public final int count;
	// first key point abscissa
	public final int x;
	// first key point ordinate
	public final int y;

	private ParabolicCurve() {
		this(0, 0, 0);
	}

	public ParabolicCurve(int x, int y) {
		this(1, x, y);
	}

	public ParabolicCurve(int count, int x, int y) {
		this.count = count;
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ParabolicCurve [count=");
		sb.append(count);
		sb.append(", x=");
		sb.append(x);
		sb.append(", y=");
		sb.append(y);
		sb.append(']');

		return sb.toString();
	}
}
