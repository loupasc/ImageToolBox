package net.fraktales.param;


public final class ContrastLevels {
	public static final ContrastLevels DEFAULT = new ContrastLevels(0, 255);
	public final int min;
	public final int max;

	public ContrastLevels(int min, int max) {
		this.min = min;
		this.max = max;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ContrastLevels [min=");
		sb.append(min);
		sb.append(", max=");
		sb.append(max);
		sb.append(']');

		return sb.toString();
	}
}
