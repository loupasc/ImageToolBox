package net.fraktales.param;

public final class MaskParameter {
	public final boolean isOpacity;
	public final String filename;
	public final int opacity;

	public MaskParameter() {
		this.isOpacity = false;
		this.filename = null;
		this.opacity = 0;
	}

	public MaskParameter(int opacity) {
		this.isOpacity = true;
		this.filename = null;
		this.opacity = opacity;
	}

	public MaskParameter(String fileName) {
		this.isOpacity = false;
		this.filename = fileName;
		this.opacity = 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("MaskParameter [isOpacity=");
		sb.append(isOpacity);
		sb.append(", filename=");
		sb.append(filename);
		sb.append(", opacity=");
		sb.append(opacity);
		sb.append(']');

		return sb.toString();
	}
}
