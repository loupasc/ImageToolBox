package net.fraktales.param;

public enum DitherQuantization {
    BINARY(2),
    WEB_SAFE(6);

    public final int colorCount;

    private DitherQuantization(int colorCount) {
        this.colorCount = colorCount;
    }
}