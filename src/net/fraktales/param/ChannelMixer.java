package net.fraktales.param;


public final class ChannelMixer {
	public static final ChannelMixer DEFAULT = new ChannelMixer(30, 59, 11);
	/** red channel % */
	public final int r;
	/** green channel % */
	public final int g;
	/** blue channel % */
	public final int b;

	public ChannelMixer(int r, int g, int b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ChannelMixer [r=");
		sb.append(r);
		sb.append(", g=");
		sb.append(g);
		sb.append(", b=");
		sb.append(b);
		sb.append(']');

		return  sb.toString();
	}
}
