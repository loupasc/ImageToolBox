package net.fraktales.param;

public enum OrderedDitherArg {
	CHECKS, DISPERSED, ARCADE, DEFAULT, LINES, RANDOM, VOID_AND_CLUSTER
}
