package net.fraktales.tools;

public interface LoggerInterface {
	public void print(char c);
	public void info(String message);
	public void error(String message);
}
