package net.fraktales.tools;


/**
 * Set of tools related to color space conversions (RGB and YUV).
 * 
 * @author louPasc
 * @version 02/11/2018
 */
public final class ColorTransformation {

	private ColorTransformation() {
	}

	/**
	 * Returns the luma corresponding to the specified RGB color.
	 * 
	 * @param r [0 .. 255]
	 * @param g [0 .. 255]
	 * @param b [0 .. 255]
	 * @param binaryPattern [0 .. 1]
	 * @return [0 .. 255]
	 */
	public static int rgbToGray(int r, int g, int b, int binaryPattern) {
		return (109 * r + 367 * g + 37 * b + 256 * binaryPattern) >> 9;
	}

	/**
	 * Returns the luma corresponding to the specified RGB color.
	 * Approximation of the formula 0.2126R + 0.7152G + 0.0722B
	 * 
	 * @param r [0 .. 255]
	 * @param g [0 .. 255]
	 * @param b [0 .. 255]
	 * @return [0 .. 65535]
	 */
	public static int rgbToDeepGray(int r, int g, int b) {
		return 55 * r + 184 * g + 18 * b;
	}

	/**
	 * Returns the Y component corresponding to the specified RGB color.
	 * 
	 * @param r [0 .. 255]
	 * @param g [0 .. 255]
	 * @param b [0 .. 255]
	 * @return [0 .. 255]
	 */
	public static double rgbToY(int r, int g, int b) {
		return 0.299 * r + 0.587 * g + 0.114 * b;
	}

	/**
	 * Returns the U component corresponding to the specified RGB color.
	 * 
	 * @param r [0 .. 255]
	 * @param g [0 .. 255]
	 * @param b [0 .. 255]
	 * @return [-111.18 .. 111.18]
	 */
	public static double rgbToU(int r, int g, int b) {
		return -0.14714 * r - 0.28886 * g + 0.436 * b;
	}

	/**
	 * Returns the V component corresponding to the specified RGB color.
	 * 
	 * @param r [0 .. 255]
	 * @param g [0 .. 255]
	 * @param b [0 .. 255]
	 * @return [-156.825 .. 156.825]
	 */
	public static double rgbToV(int r, int g, int b) {
		return 0.615 * r - 0.51499 * g - 0.10001 * b;
	}

	/**
	 * Returns the red component corresponding to the specified YUV color.
	 * 
	 * @param y [0 .. 255]
	 * @param v [-156.825 .. 156.825]
	 * @return [0 .. 255]
	 */
	public static double yuvToR(double y, double v) {
		return y + 1.139827 * v; // 0.587/0.51499
	}

	/**
	 * Returns the green component corresponding to the specified YUV color.
	 * 
	 * @param y [0 .. 255]
	 * @param u [-111.18 .. 111.18]
	 * @param v [-156.825 .. 156.825]
	 * @return [0 .. 255]
	 */
	public static double yuvToG(double y, double u, double v) {
		return y - 0.39465 * u - 0.5806 * v;
	}

	/**
	 * Returns the blue component corresponding to the specified YUV color.
	 * 
	 * @param y [0 .. 255]
	 * @param u [-111.18 .. 111.18]
	 * @return [0 .. 255]
	 */
	public static double yuvToB(double y, double u) {
		return y + 2.032078 * u; // 0.299/0.14714
	}

	/**
	 * Returns the parabola function value of input.<br>
	 * 
	 * @param cpx x control point
	 * @param cpy y control point
	 * @param intensity [0 .. 25500]
	 * @return [0 .. 25500]
	 */
	public static int parabola(int cpx, int cpy, int intensity) {
		// Saved in long since clipping may occur
		long temp = intensity;
		// Parabolic curve ax²+bx+c
		// P(0)=0 and P(255)=255, P(x1)=y1 defined by the user (x1 = cpx and y1 = cpy)
		//       (y1 - x1)         x1² - 255y1
		// a = -------------, b = -------------
		//     x1*(x1 - 255)      x1*(x1 - 255)
		temp = (((cpy - cpx) * temp * temp) / 100 + (cpx * cpx - 255 * cpy) * temp)
				/ (cpx * (cpx - 255));

		return (int) temp;
	}
	
	// -- TEST --
//	public static void main(String[] args) throws IOException {
//		BufferedImage input = ImageIO.read(new File("ChromaCube.bmp"));
//		BufferedImage output = new BufferedImage(input.getWidth(), input.getHeight(), BufferedImage.TYPE_INT_RGB);
//		for (int j = 0; j < input.getHeight(); j++) {
//			for (int i = 0; i < input.getWidth(); i++) {
//				int pixelColor = input.getRGB(i, j);
//				int r = BufferedImageUtils.getRed(pixelColor);
//				int g = BufferedImageUtils.getGreen(pixelColor);
//				int b = BufferedImageUtils.getBlue(pixelColor);
//				double y = rgbToY(r, g, b);
//				double u = rgbToU(r, g, b);
//				double v = rgbToV(r, g, b);
//
//				r = (int) Math.round(yuvToR(y, v));
//				g = (int) Math.round(yuvToG(y, u, v));
//				b = (int) Math.round(yuvToB(y, u));
//
//				output.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
//			}
//		}
//		ImageIO.write(output, "bmp", new File("output.bmp"));
//	}
}
