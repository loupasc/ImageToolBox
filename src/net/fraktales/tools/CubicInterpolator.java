package net.fraktales.tools;

public final class CubicInterpolator {

	private CubicInterpolator() {
	}

	//[p0][..][p1][XX][p2][..][p3]
	public static double applyCenterInterpolation(double[] p) {
		return (-p[0] + 9*p[1] + 9*p[2] - p[3]) / 16;
	}

	//[p0][..][..][..][p1][XX][..][..][p2][..][..][..][p3]
	public static double applyBorderInterpolation(double[] p) {
		return (-9*p[0] + 111*p[1] + 29*p[2] - 3*p[3]) / 128;
	}

	public static double[] createArrayOfPoints() {
		return new double[4];
	}
}
