package net.fraktales.tools;

import net.fraktales.image.Canvas;
import net.fraktales.param.KernelShape;

public final class SincFilter {

	private SincFilter() {
	}

	private static double lanczos(double x, int lobeCount) {
		if (x == 0.0) {
			return 1.0;
		}
		double xMultiplyPI = Math.PI * x;
		return lobeCount * Math.sin(xMultiplyPI) * Math.sin(xMultiplyPI / lobeCount) / (xMultiplyPI * xMultiplyPI);
	}

	private static double lanczos(int n, int lobeCount, int radius) {
		if (n == 0) {
			return 1.0;
		}
		if ((lobeCount * n) % radius == 0) {
			return 0.0;
		}
		// normalize into lanczos window
		return lanczos((double) lobeCount * n / radius, lobeCount);
	}

	private static double lanczos(int i, int j, int lobeCount, int radius) {
		if (i == 0) {
			// lanczos(i, lobeCount, radius) = 1
			return lanczos(j, lobeCount, radius);
		}
		if (j == 0) {
			// lanczos(j, lobeCount, radius) = 1
			return lanczos(i, lobeCount, radius);
		}
		// i and j are not equal to zero
		if (((lobeCount * i) % radius == 0) || ((lobeCount * j) % radius == 0)) {
			return 0.0;
		}

		return lanczos(i, lobeCount, radius) * lanczos(j, lobeCount, radius);
	}

	// Returns the matrix coefficient at (i,j) in function of kernel properties
	// - shape - number of lobes - radius -
	private static double computeLanczosCoef(int i, int j, KernelShape shape, int lobeCount, int radius) {

		if (shape == KernelShape.SQUARE) {
			return lanczos(i, j, lobeCount, radius);
		}

		if (shape == KernelShape.DISC) {
			if ((i == 0) || (j == 0)) {
				return lanczos(i, j, lobeCount, radius);
			}
			double d = Math.sqrt(i * i + j * j);
			if (d < radius) {
				// normalize into lanczos window
				return lanczos(lobeCount * d / radius, lobeCount);
			}
		}

		if (shape == KernelShape.DIAMOND) {
			// Diamond (45 degrees rotated square and SQRT_2-rescaled)
			int p = (i + j);
			if (p < radius) {
				return lanczos(p, i - j, lobeCount, radius);
			}
		}

		return 0.0;
	}

	// Matrix content has symmetries on horizontal axis and vertical axis
	// => Only a quarter of the matrix is generated
	// => Possibility to factorize computation allowing to divide number of multiplications by 4
	public static double[][] createLanczosKernel(KernelShape shape, int lobeCount, int radius) {
		double[][] matrix = new double[radius][radius];

		for (int j = 0; j < radius; j++) {
			for (int i = 0; i < radius; i++) {
				matrix[i][j] = computeLanczosCoef(i, j, shape, lobeCount, radius);
			}
		}

		return matrix;
	}

	public static double getCoefficientSum(double[][] m) {
		// Assuming dealing with square matrix
		int radius = m.length;
		// Let's start with the center coefficient
		double result = m[0][0];

		// Next loop in the central axis
		for (int i = 1; i < radius; i++) {
			result = result + 4 * m[i][0];
		}

		// Finally the remaining area
		for (int j = 1; j < radius; j++) {
			for (int i = 1; i < radius; i++) {
				result = result + 4 * m[i][j];
			}
		}

		return result;
	}

	/**
	 * Returns the RGB pixel color stored in an array of floating-point.
	 * 
	 * @param canvas image surface
	 * @param x pixel location
	 * @param y pixel location
	 * @param isCentralArea true if pixel are located in the central area of the surface
	 * @param m convolution matrix
	 * @param radius convolution radius
	 * @return floating-point array
	 */
	public static double[] convolve(Canvas canvas, int x, int y, boolean isCentralArea, double[][] m, int radius) {
		// Let's start with the center coefficient
		int c1 = canvas.getRGB(x, y);
		double[] rgb = { BufferedImageUtils.getRed(c1) * m[0][0],
				BufferedImageUtils.getGreen(c1) * m[0][0],
				BufferedImageUtils.getBlue(c1) * m[0][0] };

		// Next loop in the central axis
		for (int i = 1; i < radius; i++) {
			double coef = m[i][0];
			int c2;
			int c3;
			int c4;
			if (isCentralArea) {
				c1 = canvas.getRGB(x - i, y);
				c2 = canvas.getRGB(x + i, y);
				c3 = canvas.getRGB(x, y - i);
				c4 = canvas.getRGB(x, y + i);
			}
			else {
				c1 = canvas.safeGetRGB(x - i, y);
				c2 = canvas.safeGetRGB(x + i, y);
				c3 = canvas.safeGetRGB(x, y - i);
				c4 = canvas.safeGetRGB(x, y + i);
			}
			rgb[0] = rgb[0] + coef * (BufferedImageUtils.getRed(c1) + BufferedImageUtils.getRed(c2) + BufferedImageUtils.getRed(c3) + BufferedImageUtils.getRed(c4));
			rgb[1] = rgb[1] + coef * (BufferedImageUtils.getGreen(c1) + BufferedImageUtils.getGreen(c2) + BufferedImageUtils.getGreen(c3) + BufferedImageUtils.getGreen(c4));
			rgb[2] = rgb[2] + coef * (BufferedImageUtils.getBlue(c1) + BufferedImageUtils.getBlue(c2) + BufferedImageUtils.getBlue(c3) + BufferedImageUtils.getBlue(c4));
		}

		// Finally the remaining area
		for (int j = 1; j < radius; j++) {
			for (int i = 1; i < radius; i++) {
				double coef = m[i][j];
				int c2;
				int c3;
				int c4;
				if (isCentralArea) {
					c1 = canvas.getRGB(x - i, y - j);
					c2 = canvas.getRGB(x + i, y - j);
					c3 = canvas.getRGB(x - i, y + j);
					c4 = canvas.getRGB(x + i, y + j);
				}
				else {
					c1 = canvas.safeGetRGB(x - i, y - j);
					c2 = canvas.safeGetRGB(x + i, y - j);
					c3 = canvas.safeGetRGB(x - i, y + j);
					c4 = canvas.safeGetRGB(x + i, y + j);
				}
				rgb[0] = rgb[0] + coef * (BufferedImageUtils.getRed(c1) + BufferedImageUtils.getRed(c2) + BufferedImageUtils.getRed(c3) + BufferedImageUtils.getRed(c4));
				rgb[1] = rgb[1] + coef * (BufferedImageUtils.getGreen(c1) + BufferedImageUtils.getGreen(c2) + BufferedImageUtils.getGreen(c3) + BufferedImageUtils.getGreen(c4));
				rgb[2] = rgb[2] + coef * (BufferedImageUtils.getBlue(c1) + BufferedImageUtils.getBlue(c2) + BufferedImageUtils.getBlue(c3) + BufferedImageUtils.getBlue(c4));
			}
		}

		return rgb;
	}

	// -- TEST --
//	public static void main(String[] args) {
//		int radius = 8;
//		double[][] matrix = createLanczosKernel(KernelShape.DISC, 4, radius);
//		for (int j = 0; j < radius; j++) {
//			System.out.println();
//			for (int i = 0; i < radius; i++) {
//				System.out.print("[" + matrix[i][j] + "]");
//			}
//			System.out.print("[CRLF]");
//		}
//	}
}
