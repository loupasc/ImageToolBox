package net.fraktales.tools;

public class ProgressLog {

	private final int[] indexToDisplay;
	private int arrayIndex;
	private long processDuration;

	public ProgressLog(int range) {
		indexToDisplay = new int[10];
		for (int i = 0; i < 10; i++) {
			indexToDisplay[i] = (i * range) / 10;
		}
		arrayIndex = 0;
		processDuration = 0;
	}

	public static void loadingImage(String filename, int width, int height) {
		StringBuilder loadingMessage = new StringBuilder("Loading ");
		loadingMessage.append(filename);
		loadingMessage.append(" [ ");
		loadingMessage.append(width);
		loadingMessage.append(" x ");
		loadingMessage.append(height);
		loadingMessage.append(" ]...");
		ConsoleLogger.getInstance().info(loadingMessage.toString());
	}

	private char[] durationToCharArray() {
		StringBuilder sb = new StringBuilder("[");
		sb.append(Double.toString(processDuration / 1000.));
		while (sb.length() > 10) {
			sb.deleteCharAt(sb.length() - 1);
		}
		while (sb.length() < 10) {
			sb.insert(1, ' ');
		}
		sb.append("s]");
		return sb.toString().toCharArray();
	}

	public void display(int index) {

		if (arrayIndex == 10) {
			processDuration = System.currentTimeMillis() - processDuration;
			arrayIndex = arrayIndex + 1;
			ConsoleLogger.getInstance().info("]");
			char[] duration = durationToCharArray();
			for (int i = 0; i < duration.length; i++) {
				ConsoleLogger.getInstance().print(duration[i]);
			}
		}

		if (arrayIndex > 10) return;

		if ((indexToDisplay[arrayIndex] == 0) && (index == 0)) {
			processDuration = System.currentTimeMillis();
			ConsoleLogger.getInstance().info("[Process...]");
			ConsoleLogger.getInstance().print('[');
			ConsoleLogger.getInstance().print('.');
			arrayIndex = arrayIndex + 1;
		}

		if (index == indexToDisplay[arrayIndex]) {
			ConsoleLogger.getInstance().print('.');
			arrayIndex = arrayIndex + 1;
		}
	}
}
