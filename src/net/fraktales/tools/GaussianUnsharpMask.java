package net.fraktales.tools;

import net.fraktales.image.Canvas;

public final class GaussianUnsharpMask {

	// Scale double -> integer
	private static final int KERNEL_COEF_SCALE = -22306;

	// Significant value in 9x9 border
	private static final int EXTREME_BORDER_COEF = (int) Math.round(KERNEL_COEF_SCALE * gaussian(0, -8 / 3.));

	// 2 * Math.exp(-6*(x^2+y^2)) + Math.exp(-9*(x^2+y^2)/8) r=1.5
	// sum(matrix) = 0
	// factor = 65536
	// 7x7 matrix
	private static final int[][] KERNEL_MATRIX = createKernelMatrix();

	/**
	 * Strength 5-bit precision
	 */
	private final int strength;

	public GaussianUnsharpMask(int strength) {
		this.strength = strength;
	}

	private static double gaussian(double x, double y) {
		double sqr = x * x + y * y;
		return 2 * Math.exp(-6 * sqr) + Math.exp(-9 * sqr / 8);
	}

	private static int[][] createKernelMatrix() {
		int[][] matrix = new int[7][7];
		int sum = 4 * EXTREME_BORDER_COEF;
		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 7; i++) {
				// support 2 => 1.5
				matrix[i][j] = (int) Math.round(KERNEL_COEF_SCALE * gaussian(2 * (i - 3)/ 3., 2 * (j - 3)/ 3.));
				sum = sum + matrix[i][j];
			}
		}
		//
		// sum(matrix) = 0
		matrix[3][3] = matrix[3][3] - sum;
		return matrix;
	}

	private static int clamp(double value) {
		if (value < 0) {
			return 0;
		}
		if (value > 255) {
			return 255;
		}
		// rounding
		return (int) Math.round(value);
	}

	private static double convolveYuvBorder(Canvas image, int x, int y) {
		int c1 = image.safeGetRGB(x, y - 4);
		int c2 = image.safeGetRGB(x - 4, y);
		int c3 = image.safeGetRGB(x + 4, y);
		int c4 = image.safeGetRGB(x, y + 4);
		double luma = EXTREME_BORDER_COEF * (ColorTransformation.rgbToY(BufferedImageUtils.getRed(c1),
				BufferedImageUtils.getGreen(c1),
				BufferedImageUtils.getBlue(c1))
				+ ColorTransformation.rgbToY(BufferedImageUtils.getRed(c2),
						BufferedImageUtils.getGreen(c2),
						BufferedImageUtils.getBlue(c2))
				+ ColorTransformation.rgbToY(BufferedImageUtils.getRed(c3),
						BufferedImageUtils.getGreen(c3),
						BufferedImageUtils.getBlue(c3))
				+ ColorTransformation.rgbToY(BufferedImageUtils.getRed(c4),
						BufferedImageUtils.getGreen(c4),
						BufferedImageUtils.getBlue(c4)));

		return luma;
	}

	private static void assertSquareMatrix(double[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			if (inputMatrix[i].length != inputMatrix.length) {
				throw new IllegalArgumentException();
			}
		}
	}

	private static double convolveKernelMatrix(double[][] inputMatrix, int x, int y, int[][] sharpenMatrix) {
		double result = 0.;

		for (int j = 0; j < 9; j++) {
			int posY = y + j - 8;
			for (int i = 0; i < 9; i++) {
				int posX = x + i - 8;
				// only coefficients inside input matrix are taken into account
				// else coefficients are considered to be equal to zero
				if ((posX >= 0) && (posX < inputMatrix.length) && (posY >= 0) && (posY < inputMatrix.length)) {
					result = result + sharpenMatrix[i][j] * inputMatrix[posX][posY];
				}
			}
		}

		return result;
	}

	// resulting matrix dimension + 8
	public static double[][] convolveKernelMatrix(double[][] inputMatrix) {
		assertSquareMatrix(inputMatrix);
		int length = inputMatrix.length + 8;// 4 + length + 4
		double[][] result = new double[length][length];
		// Initialize sharpen matrix
		int[][] sharpenMatrix = new int[9][9];
		for (int j = 1; j < 8; j++) {
			for (int i = 1; i < 8; i++) {
				sharpenMatrix[i][j] = KERNEL_MATRIX[i - 1][j - 1];
			}
		}
		sharpenMatrix[4][0] = EXTREME_BORDER_COEF;
		sharpenMatrix[0][4] = EXTREME_BORDER_COEF;
		sharpenMatrix[8][4] = EXTREME_BORDER_COEF;
		sharpenMatrix[4][8] = EXTREME_BORDER_COEF;

		sharpenMatrix[4][4] = sharpenMatrix[4][4] + (1 << 17);// strength=0.5
		// Convolve input with sharpen
		for (int j = 0; j < length; j++) {
			for (int i = 0; i < length; i++) {
				result[i][j] = Math.scalb(convolveKernelMatrix(inputMatrix, i, j, sharpenMatrix), -17);
			}
		}

		return result;
	}

	public static int quickConvolve(Canvas image, int x, int y) {
		int centerPixelColor = image.getRGB(x, y);
		int r = BufferedImageUtils.getRed(centerPixelColor);
		int g = BufferedImageUtils.getGreen(centerPixelColor);
		int b = BufferedImageUtils.getBlue(centerPixelColor);
		// 1-bit strength (0.5) and factor 2^16 => 2^17
		double sharpenedPixel = Math.scalb(ColorTransformation.rgbToY(r, g, b), 17);
		double u = ColorTransformation.rgbToU(r, g, b);
		double v = ColorTransformation.rgbToV(r, g, b);

		sharpenedPixel = sharpenedPixel + convolveYuvBorder(image, x, y);

		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 7; i++) {
				int pixelColor = image.safeGetRGB(x - 3 + i, y - 3 + j);
				double luma = ColorTransformation.rgbToY(BufferedImageUtils.getRed(pixelColor),
						BufferedImageUtils.getGreen(pixelColor),
						BufferedImageUtils.getBlue(pixelColor));
				// strength <=> x1
				sharpenedPixel = sharpenedPixel + luma * KERNEL_MATRIX[i][j];
			}
		}

		sharpenedPixel = Math.scalb(sharpenedPixel, -17);
		r = clamp(ColorTransformation.yuvToR(sharpenedPixel, v));
		g = clamp(ColorTransformation.yuvToG(sharpenedPixel, u, v));
		b = clamp(ColorTransformation.yuvToB(sharpenedPixel, u));

		return BufferedImageUtils.packRgbBytes(r, g, b);
	}

	public int convolveYUV(Canvas image, int x, int y) {
		int centerPixelColor = image.getRGB(x, y);
		int r = BufferedImageUtils.getRed(centerPixelColor);
		int g = BufferedImageUtils.getGreen(centerPixelColor);
		int b = BufferedImageUtils.getBlue(centerPixelColor);
		// strength 2^5 and factor 2^16 => 2^21
		double sharpenedPixel = Math.scalb(ColorTransformation.rgbToY(r, g, b), 21);
		double u = ColorTransformation.rgbToU(r, g, b);
		double v = ColorTransformation.rgbToV(r, g, b);

		sharpenedPixel = sharpenedPixel + convolveYuvBorder(image, x, y) * strength;

		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 7; i++) {
				int pixelColor = image.safeGetRGB(x - 3 + i, y - 3 + j);
				double luma = ColorTransformation.rgbToY(BufferedImageUtils.getRed(pixelColor),
						BufferedImageUtils.getGreen(pixelColor),
						BufferedImageUtils.getBlue(pixelColor));
				sharpenedPixel = sharpenedPixel + luma * KERNEL_MATRIX[i][j] * strength;
			}
		}

		sharpenedPixel = Math.scalb(sharpenedPixel, -21);
		r = clamp(ColorTransformation.yuvToR(sharpenedPixel, v));
		g = clamp(ColorTransformation.yuvToG(sharpenedPixel, u, v));
		b = clamp(ColorTransformation.yuvToB(sharpenedPixel, u));

		return BufferedImageUtils.packRgbBytes(r, g, b);
	}

	private static int clamp(long value) {
		if (value < 0) {
			return 0;
		}
		if (value > 535822335) {
			return 255;
		}
		// rounding
		return (int) ((value + 1048576) >> 21);
	}

	private static int[] convolveRgbBorder(Canvas image, int x, int y) {
		int[] rgb = { 0, 0, 0 };
		int c1 = image.safeGetRGB(x, y - 4);
		int c2 = image.safeGetRGB(x - 4, y);
		int c3 = image.safeGetRGB(x + 4, y);
		int c4 = image.safeGetRGB(x, y + 4);

		rgb[0] = EXTREME_BORDER_COEF * (BufferedImageUtils.getRed(c1)
				+ BufferedImageUtils.getRed(c2)
				+ BufferedImageUtils.getRed(c3)
				+ BufferedImageUtils.getRed(c4));
		rgb[1] = EXTREME_BORDER_COEF * (BufferedImageUtils.getGreen(c1)
				+ BufferedImageUtils.getGreen(c2)
				+ BufferedImageUtils.getGreen(c3)
				+ BufferedImageUtils.getGreen(c4));
		rgb[2] = EXTREME_BORDER_COEF * (BufferedImageUtils.getBlue(c1)
				+ BufferedImageUtils.getBlue(c2)
				+ BufferedImageUtils.getBlue(c3)
				+ BufferedImageUtils.getBlue(c4));

		return rgb;
	}

	public int convolveRGB(Canvas image, int x, int y) {
		int centerPixelColor = image.getRGB(x, y);
		// strength 2^5
		// factor 2^16
		// 21-bit scale
		long r = BufferedImageUtils.getRed(centerPixelColor) << 21;
		long g = BufferedImageUtils.getGreen(centerPixelColor) << 21;
		long b = BufferedImageUtils.getBlue(centerPixelColor) << 21;

		int[] rgb = convolveRgbBorder(image, x, y);
		r = r + rgb[0] * strength;
		g = g + rgb[1] * strength;
		b = b + rgb[2] * strength;

		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 7; i++) {
				long coef = KERNEL_MATRIX[i][j] * strength;
				int pixelColor = image.safeGetRGB(x - 3 + i, y - 3 + j);
				r = r + coef * BufferedImageUtils.getRed(pixelColor);
				g = g + coef * BufferedImageUtils.getGreen(pixelColor);
				b = b + coef * BufferedImageUtils.getBlue(pixelColor);
			}
		}

		return BufferedImageUtils.packRgbBytes(clamp(r), clamp(g), clamp(b));
	}

	// -- TEST --
//	public static void main(String[] args) {
//		System.out.println("[" + EXTREME_BORDER_COEF + "]");
//		int sum = 4 * EXTREME_BORDER_COEF;
//		for (int j = 0; j < 7; j++) {
//			System.out.println();
//			for (int i = 0; i < 7; i++) {
//				System.out.print("[" + KERNEL_MATRIX[i][j] + "]");
//				sum = sum + KERNEL_MATRIX[i][j];
//			}
//		}
//		System.out.println();
//		System.out.println(sum);
//		double[][] m = convolveKernelMatrix(new double[][] {{ 0.0625, 0.125, 0.0625 }, { 0.125, 0.25, 0.125 }, { 0.0625, 0.125, 0.0625 }});
//		double s = 0.;
//		for (int j = 0; j < m.length; j++) {
//			System.out.println();
//			for (int i = 0; i < m.length; i++) {
//				System.out.print("[" + m[i][j] + "]");
//				s = s + m[i][j];
//			}
//		}
//		System.out.println();
//		System.out.println(s);
//	}
}
