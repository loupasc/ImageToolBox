package net.fraktales.tools;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import net.fraktales.image.Canvas;

public final class BufferedImageUtils {

	private BufferedImageUtils() {
	}

	private static Raster getRaster(File file) throws IOException {
		Raster result = null;
		ImageInputStream stream = ImageIO.createImageInputStream(file);

		Iterator<ImageReader> iterator = ImageIO.getImageReaders(stream);
		while (iterator.hasNext()) {
			ImageReader reader = iterator.next();
			reader.setInput(stream);
			try {
				// Single image (0) and No parameter (null)
				result = reader.readRaster(0, null);
			} catch (IOException e) {
				// Try to get raster from the next image reader (if any)
			}
		}

		return result;
	}

	private static BufferedImage createRgbImage(Raster raster) {
		BufferedImage image = null;

		if (raster != null) {
			// Assuming image is a CMYK JPEG
			// ByteInterleavedRaster CMYK bands
			if ((raster.getDataBuffer().getDataType() == DataBuffer.TYPE_BYTE) && (raster.getNumBands() == 4)) {
				byte[] rasterBitmap = ((DataBufferByte) raster.getDataBuffer()).getData();
				image = new BufferedImage(raster.getWidth(), raster.getHeight(), BufferedImage.TYPE_INT_RGB);
				int[] bitmap = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
				for (int i = 0; i < bitmap.length; i++) {
					// 0 C
					// 1 M
					// 2 Y
					// 3 K
					int k = rasterBitmap[4 * i + 3] & 0xFF;
					int r = (k * (rasterBitmap[4 * i] & 0xFF)) / 255;
					int g = (k * (rasterBitmap[4 * i + 1] & 0xFF)) / 255;
					int b = (k * (rasterBitmap[4 * i + 2] & 0xFF)) / 255;
					bitmap[i] = b | g << 8 | r << 16;
				}
			}
		}

		return image;
	}

	private static int packRgbBytes(byte r, byte g, byte b) {
		return packRgbBytes(r & 0xFF, g & 0xFF, b & 0xFF);
	}

	private static int prepare8BitConversion(int value, int ditherPattern) {
		return value + (ditherPattern << 7);
	}

	private static int add16BitNoise(int value, int ditherPattern) {
		// MAX_VALUE 65535. MAX_VALUE + noise 65663
		// Avoid int to long to int
		// 65535 * 2/5 = 26214
		// 65663 * 2/5 = 26265.2
		return (prepare8BitConversion(value, ditherPattern) * 26214) / 26265;
	}

	/**
	 * Converts 16-bit depth intensity to 8-bit with checkerboard dithering.
	 * 
	 * @param value 16-bit depth intensity
	 * @param ditherPattern checkerboard dithering
	 * @return 8-bit depth intensity
	 */
	private static int convertTo8Bit(short value, int ditherPattern) {
		return (prepare8BitConversion(value & 0xFFFF, ditherPattern) << 1) / 513;
	}

	private static boolean is48BitPerPixel(BufferedImage image) {
		return (image.getType() == BufferedImage.TYPE_CUSTOM)
				&& (image.getColorModel().getColorSpace().isCS_sRGB())
				&& (image.getColorModel().getPixelSize() == 48)
				&& (image.getColorModel().getNumComponents() == 3);
	}

	private static BufferedImage convertToRgb(BufferedImage input) {
		// BufferedImage.TYPE_3BYTE_BGR
		// Code could be more basic but this image format is common and the processing is simple and efficient.
		// BufferedImage.TYPE_BYTE_GRAY and BufferedImage.TYPE_USHORT_GRAY
		// No transparency, no color management : simple and efficient
		// Transparency and color management shall obviously be delegated to BufferedImage
		int width = input.getWidth();
		int height = input.getHeight();
		BufferedImage canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int[] canvasSurface = ((DataBufferInt) canvas.getRaster().getDataBuffer()).getData();

		if (input.getType() == BufferedImage.TYPE_3BYTE_BGR) {
			byte[] bitmap = ((DataBufferByte) input.getRaster().getDataBuffer()).getData();
			int i = 0;
			for (int pos = 0; pos < bitmap.length; pos = pos + 3) {
				canvasSurface[i] = packRgbBytes(bitmap[pos + 2], bitmap[pos + 1], bitmap[pos]);
				i = i + 1;
			}
		}
		else if (input.getType() == BufferedImage.TYPE_BYTE_GRAY) {
			byte[] bitmap = ((DataBufferByte) input.getRaster().getDataBuffer()).getData();
			for (int i = 0; i < bitmap.length; i++) {
				canvasSurface[i] = packRgbBytes(bitmap[i], bitmap[i], bitmap[i]);
			}
		}
		else if (input.getType() == BufferedImage.TYPE_USHORT_GRAY) {
			short[] bitmap = ((DataBufferUShort) input.getRaster().getDataBuffer()).getData();
			int offset = 0;
			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					byte luma8bit = (byte) convertTo8Bit(bitmap[offset], (i ^ j) & 0x1);
					canvasSurface[offset] = packRgbBytes(luma8bit, luma8bit, luma8bit);
					offset = offset + 1;
				}
			}
		}
		else if (is48BitPerPixel(input)) {
			int[] p = new int[3];
			// RGB arrangements is decided by SampleModel and ColorModel
			short[] components = new short[3];
			int offset = 0;
			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					//
					// This pixel picking method is slow but it seems to give accurate value
					//
					input.getRaster().getSampleModel().getPixel(i, j, p, input.getRaster().getDataBuffer());

					components[0] = (short) add16BitNoise(p[0], (i ^ j) & 0x1);
					components[1] = (short) add16BitNoise(p[1], (i ^ j) & 0x1);
					components[2] = (short) add16BitNoise(p[2], (i ^ j) & 0x1);

					canvasSurface[offset] = packRgbBytes(input.getColorModel().getRed(components),
							input.getColorModel().getGreen(components),
							input.getColorModel().getBlue(components));

					offset = offset + 1;
				}
			}
		}
		else {
			int[] line = new int[width];
			int offset = 0;
			for (int j = 0; j < height; j++) {
				input.getRGB(0, j, line.length, 1, line, 0, line.length);
				for (int i = 0; i < width; i++) {
					int pixelColor = line[i];
					int opacity = pixelColor >>> 24;

					canvasSurface[offset] = packRgbBytes(((pixelColor >> 16 & 0xFF) * opacity) / 255,
							((pixelColor >> 8 & 0xFF) * opacity) / 255,
							((pixelColor & 0xFF) * opacity) / 255);

					offset = offset + 1;
				}
			}
		}

		return canvas;
	}

	/**
	 * Stores RGB 8-bit components into a 32-bit integer.
	 * 
	 * @param r red
	 * @param g green
	 * @param b blue
	 * @return an 32-bit integer
	 */
	public static int packRgbBytes(int r, int g, int b) {
		return b | g << 8 | r << 16;
	}

	/**
	 * Returns the red intensity [0 .. 255]
	 * 
	 * @param rgb RGB pixel
	 * @return red intensity
	 */
	public static int getRed(int rgb) {
		return rgb >> 16;
	}

	/**
	 * Returns the green intensity [0 .. 255]
	 * 
	 * @param rgb RGB pixel
	 * @return green intensity
	 */
	public static int getGreen(int rgb) {
		return rgb >> 8 & 0xFF;
	}

	/**
	 * Returns the blue intensity [0 .. 255]
	 * 
	 * @param rgb RGB pixel
	 * @return blue intensity
	 */
	public static int getBlue(int rgb) {
		return rgb & 0xFF;
	}

	/**
	 * Returns the binary red intensity
	 * @param intensity 8-bit value
	 * @return binary intensity
	 */
	public static byte getBinaryRed(int rgb) {
		if (getRed(rgb) < 128) {
			return 0;
		}
		return 4;
	}

	/**
	 * Returns the binary green intensity
	 * @param intensity 8-bit value
	 * @return binary intensity
	 */
	public static byte getBinaryGreen(int rgb) {
		if (getGreen(rgb) < 128) {
			return 0;
		}
		return 2;
	}

	/**
	 * Returns the binary blue intensity
	 * @param intensity 8-bit value
	 * @return binary intensity
	 */
	public static byte getBinaryBlue(int rgb) {
		if (getBlue(rgb) < 128) {
			return 0;
		}
		return 1;
	}

	/**
	 * Returns the Canvas represented by a TYPE_INT_RGB BufferedImage
	 * @param file
	 * @return a TYPE_INT_RGB BufferedImage
	 * @throws IOException
	 */
	public static Canvas createCanvasFromFile(File file) throws IOException {
		BufferedImage rgbImage = null;

		if (file.exists()) {
			try {
				BufferedImage image = ImageIO.read(file);
				if (image != null) {
					rgbImage = convertToRgb(image);
				}
			}
			catch (IndexOutOfBoundsException e) {
				throw new IOException("Severe error occurred while reading data of the image " + file + ", " + e.getMessage() + " - you should change image format and retry");
			}
			catch (IOException e) {
				// Reading image format failed
				// Try to get image raster
				Raster raster = getRaster(file);
				rgbImage = createRgbImage(raster);
			}

			if (rgbImage == null) {
				throw new IOException("Cannot decode image " + file + " - file format not supported");
			}
		}
		else {
			throw new IOException("Cannot find " + file);
		}

		return new Canvas(rgbImage);
	}

	public static BufferedImage createIndexedBufferedImage(int width, int height) {
		byte[] redPalette = { 0,0,0,0,(byte) 255,(byte) 255,(byte) 255,(byte) 255,0,0,0,0,0,0,0,0 };
		byte[] greenPalette = { 0,0,(byte) 255,(byte) 255,0,0,(byte) 255,(byte) 255,0,0,0,0,0,0,0,0 };
		byte[] bluePalette = { 0,(byte) 255,0,(byte) 255,0,(byte) 255,0,(byte) 255,0,0,0,0,0,0,0,0 };

		IndexColorModel colorLookUp = new IndexColorModel(4, 16, redPalette, greenPalette, bluePalette);
		return new BufferedImage(width, height, BufferedImage.TYPE_BYTE_INDEXED, colorLookUp);
	}

//	public static void main(String[] args) throws IOException {
//		BufferedImage indexedColorImage = createIndexedBufferedImage(80, 60);
//		byte[] bitmap = ((DataBufferByte) indexedColorImage.getRaster().getDataBuffer()).getData();
//		bitmap[0] = 119;
//		bitmap[80] = 64;
//		ImageIO.write(indexedColorImage, "bmp", new File("indexed.bmp"));
//	}
}
