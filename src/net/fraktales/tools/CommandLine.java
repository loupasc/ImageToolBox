package net.fraktales.tools;

import net.fraktales.parser.AbstractParser;
import net.fraktales.parser.BlockifierParser;
import net.fraktales.parser.BlurParser;
import net.fraktales.parser.ColorizerParser;
import net.fraktales.parser.ContrastMatcherParser;
import net.fraktales.parser.DefaultParser;
import net.fraktales.parser.DitheringParser;
import net.fraktales.parser.Filter;
import net.fraktales.parser.GradientMapperParser;
import net.fraktales.parser.HalftoningParser;
import net.fraktales.parser.ImageMixingParser;
import net.fraktales.parser.ImageProcessingParser;
import net.fraktales.parser.LevelsAutoParser;
import net.fraktales.parser.SharpenParser;
import net.fraktales.parser.SigmoidalContrastParser;
import net.fraktales.parser.SincParser;
import net.fraktales.parser.UpscalerParser;

public class CommandLine {

	private final String[] args;
	private AbstractParser parser;

	public CommandLine(String[] args) {
		this.args = args.clone();
		parser = new DefaultParser();
	}

	private static boolean isParameterArrayEmpty(String[] args) {
		if ((args == null) || (args.length == 0)) {
			return true;
		}
		return false;
	}

	public boolean parse() {

		final String output = "output.bmp";

		if (isParameterArrayEmpty(args)) {
			// Nothing to do
		}
		else if (args[0].compareToIgnoreCase(Filter.BLOCKIFIER.param) == 0) {
			parser = new BlockifierParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.BLUR.param) == 0) {
			parser = new BlurParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.COLORIZER.param) == 0) {
			parser = new ColorizerParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.GRADIENT_MAPPER.param) == 0) {
			parser = new GradientMapperParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.HALFTONING.param) == 0) {
			parser = new HalftoningParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.DITHER.param) == 0) {
			parser = new DitheringParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.LEVELS_AUTO.param) == 0) {
			parser = new LevelsAutoParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.MATCHER.param) == 0) {
			parser = new ContrastMatcherParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.MIXING.param) == 0) {
			parser = new ImageMixingParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.SHARPEN.param) == 0) {
			parser = new SharpenParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.SINC.param) == 0) {
			parser = new SincParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.SIGMOIDAL_CONTRAST.param) == 0) {
			parser = new SigmoidalContrastParser(output);
		}
		else if (args[0].compareToIgnoreCase(Filter.UPSCALER2X.param) == 0) {
			parser = new UpscalerParser(output);
		}
		else if (!args[0].startsWith("-")) {
			// The first argument is the file name
			parser = new ImageProcessingParser(output);
		}

		return parser.parse(args);
	}

	public AbstractParser getParser() {
		return parser;
	}
}
