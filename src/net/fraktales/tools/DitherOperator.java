package net.fraktales.tools;

public class DitherOperator {

	//
	// Matrix extract from void-and-cluster algorithm.
	// The following 256 coefficients have been got from that algorithm results.
	//
	private static final int[] DITHER_MAP = new int[] {
		133,  43, 161, 232, 138, 109,  41, 247,  97, 217, 118, 204, 103,  62, 224, 108, 
		 15, 238,  86,  61,  24, 201, 158,   5, 173,  32, 154,  16, 135, 166,  36, 182, 
		149, 200, 125, 171, 245,  71, 100, 223, 130,  68, 244,  54, 196,  83, 246,  70, 
		 99,  55,   4, 212,  35, 140, 179,  47,  89, 209, 180,  96, 231,   3, 123, 211, 
		 25, 242, 153,  95, 116, 194,  17, 248, 152,  13, 120,  37, 139, 177,  45, 156, 
		110, 184,  75, 218,  53, 236,  74, 107, 190,  57, 234, 164,  67, 216,  88, 228, 
		 60, 132,  31, 170,  11, 159, 131,  38, 219, 137, 102,   9, 195, 112,  20, 191, 
		  6, 249, 198, 117, 230,  91, 213, 167,  22,  72, 176, 250, 142,  52, 239, 148, 
		175,  98,  48,  80, 146,  34,  63, 115, 241, 199,  39,  87,  29, 207, 124,  76, 
		225, 134, 210,  18, 172, 251, 193,   2,  92, 151, 122, 226, 160,  93, 183,  40, 
		 23, 162,  59, 233, 111,  78, 129, 163, 222,  56,  14, 188,  64,   8, 214, 113, 
		243,  90, 127, 186,  12, 206,  51,  33, 197,  85, 252, 136, 104, 240, 141,  69, 
		181,   1, 220,  42,  94, 155, 237, 143, 106, 174,  30, 208,  44, 165,  26, 202, 
		105, 147,  77, 169, 253, 119,  73,   7, 229,  66, 150, 114,  79, 227, 121,  49, 
		254,  28, 215, 128,  58,  27, 178, 203, 126,  19, 235, 168,   0, 189,  84, 157, 
		 65, 192, 101,  10, 187, 221,  82, 145,  50, 185,  81,  46, 255, 144,  21, 205
	};
	//
	// Non-rectangular dither tile (it's why coefficients appear several times)
	// [   4   ]
	// [ 3 0 1 ]
	// [   2   ]
	// Coefficients are stored in a square matrix for performance purpose.
	//
	//private static final int[] PLUS_DITHER_MAP = new int[] {
	//	4, 3, 0, 1, 2, 
	//	0, 1, 2, 4, 3, 
	//	2, 4, 3, 0, 1, 
	//	3, 0, 1, 2, 4, 
	//	1, 2, 4, 3, 0
	//};
	// coef * 2 + (0 or 1)
	private static final int[] PLUS_DITHER_MAP = new int[] {
		8, 9, 6, 7, 1, 0, 3, 2, 4, 5,
		9, 8, 2, 3, 0, 1, 4, 5, 7, 6,
		1, 0, 3, 2, 4, 5, 8, 9, 6, 7,
		0, 1, 4, 5, 7, 6, 9, 8, 2, 3,
		4, 5, 8, 9, 6, 7, 1, 0, 3, 2,
		7, 6, 9, 8, 2, 3, 0, 1, 4, 5,
		6, 7, 1, 0, 3, 2, 4, 5, 8, 9,
		2, 3, 0, 1, 4, 5, 7, 6, 9, 8,
		3, 2, 4, 5, 8, 9, 6, 7, 1, 0,
		4, 5, 7, 6, 9, 8, 2, 3, 0, 1
	};
	/** Maximum intensity a pixel can take. */
	private final int maxValue;

	/**
	 * Constructs an instance of dither operator.
	 * 
	 * @param maxValue maximum intensity a pixel can take
	 */
	public DitherOperator(int maxValue) {
		this.maxValue = maxValue;
	}

	//
	// Internal method for applying dither process
	//
	private static int applyDither(int luma8bit, int colorCount, int coefCount, int dither) {
		int pseudoColorCount = (colorCount - 1) * coefCount + 1;
		// WHITE + MAX_DITHER = colorCount * coefCount => overflow
		// Substract one is a cheap way to resolve the clipping issue
		return (pseudoColorCount * luma8bit + 255 * dither - 1) / (255 * coefCount);
	}

	/**
	 * Static. Apply checks dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyChecksDithering(int luma8bit, int colorCount, int x, int y) {
		int ditherPattern = (x ^ y) & 0x1;
		return applyDither(luma8bit, colorCount, 2, ditherPattern);
	}

	/**
	 * Static. Apply dispersed dot dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyDispersedDotDithering(int luma8bit, int colorCount, int x, int y) {
		int ditherPattern = (((x ^ y) & 0x1) << 1) | (y & 0x1);
		return applyDither(luma8bit, colorCount, 4, ditherPattern);
	}

	/**
	 * Static. Apply arcade dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyArcadeDithering(int luma8bit, int colorCount, int y) {
		int ditherPattern = 2 + (y & 0x3);
		return applyDither(luma8bit, colorCount, 8, ditherPattern);
	}

	/**
	 * Returns the matrix coefficient at the specified location.
	 * 
	 * @param x pixel location
	 * @param y pixel location
	 * @return an integer in [0; 4]
	 */
	private static int getTilesDitherCoef(int x, int y) {
		return PLUS_DITHER_MAP[(20 * (y % 5)) + 2 * (x % 5)] >> 1;
	}

	/**
	 * Returns the matrix coefficient at the specified location.
	 * 
	 * @param x pixel location
	 * @param y pixel location
	 * @return an integer in [0; 9]
	 */
	private static int getLinesDitherCoef(int x, int y) {
		return PLUS_DITHER_MAP[(10 * (y % 10)) + (x % 10)];
	}

	/**
	 * Static. Apply tiles dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyTilesDithering(int luma8bit, int colorCount, int x, int y) {
		return applyDither(luma8bit, colorCount, 5, getTilesDitherCoef(x, y));
	}

	/**
	 * Static. Apply lines dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyLinesDithering(int luma8bit, int colorCount, int x, int y) {
		return applyDither(luma8bit, colorCount, 10, getLinesDitherCoef(x, y));
	}

	/**
	 * Static. Apply tiles dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyVoidAndClusterDithering(int luma8bit, int colorCount, int x, int y) {
		return applyDither(luma8bit, colorCount, 256, DITHER_MAP[((y & 0xF) << 4) + (x & 0xF)]);
	}

	/**
	 * Static. Apply pseudorandom dithering on 8-bit image only (maxValue fixed to 255).
	 * 
	 * @param luma8bit pixel intensity
	 * @param colorCount number of colors per channel
	 * @param x pixel location
	 * @param y pixel location
	 * @return pixel intensity between 0 inclusive and colorCount exclusive
	 */
	public static int applyRandomDithering(int luma8bit, int colorCount, int x, int y) {
		// mix x and y bits into seed
		int seed = (Integer.reverse(y) >>> 23) << 22 | Integer.reverse(x) >>> 10;
		// multiplicative congruential generator
		int ditherPattern = (int) ((48271L * seed) % Integer.MAX_VALUE);
		//return applyDither(luma8bit, colorCount, 255, ditherPattern / 8421506);
		return ((colorCount - 1) * luma8bit + (ditherPattern / 8421506)) / 255;
	}

	/**
	 * Static. This method returns a value between 0 and amount following a blue noise pattern.
	 * 
	 * @param amount noise strength
	 * @param x pixel location
	 * @param y pixel location
	 * @return an integer between 0 and amount
	 */
	public static int blueNoise(int amount, int x, int y) {
		long result = (amount * (long) DITHER_MAP[((y & 0xF) << 4) + (x & 0xF)]) >> 8;
		return (int) result;
	}

	// out: [0; 255]
	public int apply8bitsDithering(int value, int x, int y) {
		// 255 * 256 = 65280
		long luma8bit = (65280L * value + maxValue * DITHER_MAP[((y & 0xF) << 4) + (x & 0xF)]) / (maxValue << 8);

		return (int) luma8bit;
	}
}
