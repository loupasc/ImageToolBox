package net.fraktales.tools;

import java.util.Arrays;

/**
 * Classe d'outils pour la manipulation d'histogramme.
 * 
 * @author louPasc
 * @version 22/03/2014
 */
public final class HistogramUtils {
	private static final int HISTOGRAM_SIZE = 256;

	private HistogramUtils() {
	}

	/**
	 * Retourne l'histogramme du plan de bits specifie en parametre.
	 * 
	 * @param bitmap
	 * @return
	 */
	public static int[] createHistogram(byte[] bitmap) {
		int[] histogram = new int[HISTOGRAM_SIZE];

		Arrays.fill(histogram, 0);
		for (int i = 0; i < bitmap.length; i++) {
			histogram[bitmap[i] & 0xFF]++;
		}

		return histogram;
	}

	/**
	 * Retourne l'histogramme cumule du plan de bits specifie en parametre.
	 * 
	 * @param bitmap
	 * @return
	 */
	public static int[] createCumulativeHistogram(byte[] bitmap) {
		int[] histogram = createHistogram(bitmap);

		for (int i = 1; i < HISTOGRAM_SIZE; i++) {
			histogram[i] = histogram[i] + histogram[i - 1];
		}

		return histogram;
	}
}
