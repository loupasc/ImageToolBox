package net.fraktales.tools;

public final class ConsoleLogger implements LoggerInterface {

	private static ConsoleLogger instance = null;

	private ConsoleLogger() {
	}

	public static ConsoleLogger getInstance() {
		if (instance == null) {
			instance = new ConsoleLogger();
		}
		return instance;
	}

	@Override
	public void print(char c) {
		System.out.print(c);
	}

	@Override
	public void info(String message) {
		System.out.println(message);
	}

	@Override
	public void error(String message) {
		System.err.println(message);
	}
}
