package net.fraktales.tools;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Non-linear contrast control based on a logistic function
 * 
 * @author loupasc
 * @version 02/11/2018
 */
public final class SigmoidFunction {

	private final double contrastFactor;
	private final double coef0;
	private final double coef1;

	/**
	 * Creates the logistic function shaped by the specified factor.
	 * 
	 * @param contrastFactor contrast factor
	 */
	public SigmoidFunction(double contrastFactor) {
		this.contrastFactor = contrastFactor;
		coef0 = computeCoef0(contrastFactor);
		coef1 = computeCoef1(contrastFactor);
	}

	/**
	 * The result is used by the first parameter of the method {@link #get(int)}.
	 * 
	 * @param contrastFactor
	 * @return
	 */
	private static double computeCoef0(double contrastFactor) {
		BigDecimal d = BigDecimal.valueOf(Math.exp(0.5 * contrastFactor));

		return BigDecimal.ONE.divide(BigDecimal.ONE.add(d), MathContext.DECIMAL128).doubleValue();
	}

	/**
	 * The result is used by the second parameter of the method {@link #get(int)}.
	 * 
	 * @param contrastFactor
	 * @return
	 */
	private static double computeCoef1(double contrastFactor) {
		BigDecimal d = BigDecimal.valueOf(Math.exp(0.5 * contrastFactor));
		BigDecimal f = BigDecimal.valueOf(Math.exp(-0.5 * contrastFactor));

		return BigDecimal.ONE.divide(BigDecimal.ONE.add(f), MathContext.DECIMAL128).subtract(BigDecimal.ONE.divide(BigDecimal.ONE.add(d), MathContext.DECIMAL128)).doubleValue();
	}

	/**
	 * Returns the sigmoidal function value of input.
	 * 
	 * @param i input [0; 25500]
	 * @return output [0; 25500]
	 */
	public int get(int i) {
		double p = i / 25500.0;

		p = 1 / (1 + Math.exp(contrastFactor * (0.5 - p)));
		p = (p - coef0) / coef1;
		return (int) Math.round(25500.0 * p);
	}
}
