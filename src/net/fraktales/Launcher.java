package net.fraktales;

import net.fraktales.processor.AbstractProcessor;
import net.fraktales.processor.ProcessorFactory;
import net.fraktales.tools.CommandLine;
import net.fraktales.tools.ConsoleLogger;

/**
 * Entry point of the ImageToolBox application.
 * 
 * @author Pascal Ollive
 * @version 1.0.1
 * @since Jun 2, 2020
 */
public final class Launcher {

	private static final int MAJOR_VERSION = 1;
	private static final int MINOR_VERSION = 0;
	private static final int BUILD_VERSION = 3;

	private Launcher() {
	}

	private static void printArgs(String[] args) {
		int argsLength = args.length;
		StringBuilder sb = new StringBuilder("[louPasc]>ImageToolBox v");
		sb.append(MAJOR_VERSION);
		sb.append('.');
		sb.append(MINOR_VERSION);
		sb.append('.');
		sb.append(BUILD_VERSION);

		for (int i = 0; i < argsLength; i++) {
			sb.append(' ');
			sb.append(args[i]);
		}
		ConsoleLogger.getInstance().info(sb.toString());
	}

	/**
	 * Entry point.
	 * 
	 * @param args application parameters
	 * @since Jun 2, 2020
	 */
	public static void main(String[] args) {

		printArgs(args);

		CommandLine commandLine = new CommandLine(args);
		boolean parseSuccessful = commandLine.parse();

		if (parseSuccessful) {
			AbstractProcessor processor = ProcessorFactory.createFromParser(commandLine.getParser());
			processor.execute();
		}
	}
}
