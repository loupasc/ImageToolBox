package net.fraktales.parser;

import java.util.Arrays;

import net.fraktales.param.GradientMapperColor;
import net.fraktales.tools.ConsoleLogger;

public class GradientMapperParser extends AbstractParser {

	private GradientMapperColor[] colors;
	private boolean softerRenderingEnabled;

	public GradientMapperParser(String outputFileName) {
		super(outputFileName);
		colors = new GradientMapperColor[0];
		softerRenderingEnabled = false;
	}

	private static GradientMapperColor[] getColorArray(String colorParam) {
		GradientMapperColor[] result = new GradientMapperColor[4];

		if (colorParam.length() != 4) {
			ConsoleLogger.getInstance().error(colorParam + " <- 4 characters are expected");
			return null;
		}
		for (int i = 0; i < 4; i++) {
			try {
				result[i] = GradientMapperColor.getColorFrom(colorParam.charAt(i));
			}
			catch (IllegalArgumentException e) {
				ConsoleLogger.getInstance().error("Unknown color " + colorParam.charAt(i));
				return null;
			}
		}

		return result;
	}

	@Override
	public Filter getFilter() {
		return Filter.GRADIENT_MAPPER;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			GradientMapperColor[] colorArray = getColorArray(args[1]);
			if (colorArray == null) {
				printArgs();
				return false;
			}
			colors = colorArray;
			setInputFileName(args[2]);
		}
		else if (args.length == 4) {
			if (args[1].compareToIgnoreCase("softer") == 0) {
				softerRenderingEnabled = true;
			}
			else {
				ConsoleLogger.getInstance().error("Invalid option " + args[1]);
				printArgs();
				return false;
			}
			GradientMapperColor[] colorArray = getColorArray(args[2]);
			if (colorArray == null) {
				printArgs();
				return false;
			}
			colors = colorArray;
			setInputFileName(args[3]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public GradientMapperColor[] getColors() {
		return colors;
	}

	public boolean isSofterRenderingEnabled() {
		return softerRenderingEnabled;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("GradientMapperParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", colors=");
		sb.append(Arrays.toString(colors));
		sb.append(", softerRenderingEnabled=");
		sb.append(softerRenderingEnabled);
		sb.append("]");

		return sb.toString();
	}
}
