package net.fraktales.parser;

public enum Filter {
	NULL(null, null),
	BLOCKIFIER("-blockify", " (softer) (tiny_pattern) <file>"),
	BLUR("-blur", " <radius 1..10> <file>"),
	COLORIZER("-colorize", " <monochrome image> <colored image>"),
	GRADIENT_MAPPER("-gradientmap", " (softer) XXXX (X=K|B|G|C|J|R|M|P|A|Y|W) <file>\n[ K:blacK B:Blue G:Green C:Cyan J:Jade R:Red M:Magenta P:Pink A:Amber Y:Yellow W:White ]"),
	HALFTONING("-ordereddither", " (checks | dispersed | arcade | default | lines | random | void_and_cluster) (binary | web_safe) <file>"),
	DITHER("-dither", " (binary | web_safe) <file>"),
	LEVELS_AUTO("-levels", " <file>"),
	MATCHER("-match", " <reference file> <file to adjust>"),
	MIXING("-mixing", " <first file> <second file> (<mask image> | <0..255>)"),
	SHARPEN("-sharpen", " <strength 0..10> (luma_only) <file>"),
	SINC("-sinc", " <square | disc | diamond> <cutoff 1..4> <radius 8..100> (contrast 0..10) <file>"),
	SIGMOIDAL_CONTRAST("-sigmoidalcontrast", " <decimal> <file>"),
	UPSCALER2X("-upscale2x", " (-sharp) <file>"),
	BW_LEVELS(null, "<file> -bw <r 0..100> <g 0..100> <b 0..100> -l <min 0..255> <max 0..255> (-pc <x 1..254> <y 0..255>) (-sc <decimal>)");

	public final String param;
	public final String usage;

	private Filter(String param, String usage) {
		this.param = param;
		this.usage = (param == null ? usage : param + usage);
	}
}
