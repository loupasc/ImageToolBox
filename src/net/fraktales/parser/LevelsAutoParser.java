package net.fraktales.parser;

public class LevelsAutoParser extends AbstractParser {

	public LevelsAutoParser(String outputFileName) {
		super(outputFileName);
	}

	@Override
	public Filter getFilter() {
		return Filter.LEVELS_AUTO;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 2) {
			setInputFileName(args[1]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("LevelsAutoParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(']');

		return sb.toString();
	}
}
