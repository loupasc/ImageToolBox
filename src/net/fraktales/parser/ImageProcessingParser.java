package net.fraktales.parser;

import net.fraktales.param.ChannelMixer;
import net.fraktales.param.ContrastLevels;
import net.fraktales.param.ParabolicCurve;
import net.fraktales.tools.ConsoleLogger;

public class ImageProcessingParser extends AbstractParser {

	private static final int PARSE_ERROR = -1;
	private ChannelMixer bw;
	private ContrastLevels levels;
	private ParabolicCurve pc;
	private double sigmoidalContrast;

	public ImageProcessingParser(String outputFile) {
		super(outputFile);
		bw = ChannelMixer.DEFAULT;
		levels = ContrastLevels.DEFAULT;
		pc = ParabolicCurve.DEFAULT;
		sigmoidalContrast = PARSE_ERROR;
	}

	private static int parseInt(String argName, String argValue) {
		int result = PARSE_ERROR;
		try {
			result = Integer.parseInt(argValue);
		}
		catch (NumberFormatException e) {
			ConsoleLogger.getInstance().error(argValue + " <- Integer expected for " + argName);
		}

		return result;
	}

	private static double parseSigmoidalConstrast(String argValue) {
		double result = PARSE_ERROR;
		try {
			result = Double.parseDouble(argValue);
			if (result <= 0) {
				ConsoleLogger.getInstance().error(argValue + " <- Positive value expected for Sigmoidal constrast");
				return PARSE_ERROR;
			}
		}
		catch (NumberFormatException e) {
			ConsoleLogger.getInstance().error(argValue + " <- Decimal expected for Sigmoidal constrast");
		}

		return result;
	}

	@Override
	public Filter getFilter() {
		return Filter.BW_LEVELS;
	}

	@Override
	public boolean parse(String[] args) {
		if ((args.length == 8) || (args.length == 10) || (args.length == 11) || (args.length == 13)) {
			setInputFileName(args[0]);

			int channelMixerR = parseInt("Red Channel Percentage", args[2]);
			int channelMixerG = parseInt("Green Channel Percentage", args[3]);
			int channelMixerB = parseInt("Blue Channel Percentage", args[4]);

			int min = parseInt("Constast level min", args[6]);
			int max = parseInt("Constast level max", args[7]);

			if ((channelMixerR == PARSE_ERROR) || (channelMixerG == PARSE_ERROR) || (channelMixerB == PARSE_ERROR)
					|| (min == PARSE_ERROR) || (max == PARSE_ERROR)) {
				printArgs();
				return false;
			}
			
			if (min == max) {
				ConsoleLogger.getInstance().error("min shall be different from max");
				return false;
			}

			if ((args.length == 10) || (args.length == 13)) {
				sigmoidalContrast = (args.length == 10 ? parseSigmoidalConstrast(args[9]) : parseSigmoidalConstrast(args[12]));
				if (sigmoidalContrast < 0) {
					// PARSE_ERROR
					printArgs();
					return false;
				}
			}

			if ((args.length == 11) || (args.length == 13)) {
				int parabolicCurveX = parseInt("Parabolic curve absciss", args[9]);
				int parabolicCurveY = parseInt("Parabolic curve ordinate", args[10]);
				if ((parabolicCurveX == PARSE_ERROR) || (parabolicCurveY == PARSE_ERROR)) {
					printArgs();
					return false;
				}
				if ((parabolicCurveX > 0) && (parabolicCurveX < 255)) {
					pc = new ParabolicCurve(parabolicCurveX, parabolicCurveY);
				}
				else {
					printArgs();
					return false;
				}
			}

			bw = new ChannelMixer(channelMixerR, channelMixerG, channelMixerB);
			levels = new ContrastLevels(min, max);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public ChannelMixer getChannelMixerParameters() {
		return bw;
	}

	public ContrastLevels getContrastLevelParameters() {
		return levels;
	}

	public ParabolicCurve getParabolicCurveParameters() {
		return pc;
	}

	public double getSigmoidalContrast() {
		return sigmoidalContrast;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ImageProcessingParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", bw=");
		sb.append(bw);
		sb.append(", l=");
		sb.append(levels);
		if (pc.count > 0) {
			sb.append(", pc=");
			sb.append(pc);
		}
		if (sigmoidalContrast > 0) {
			sb.append(", sigmoidalContrast=");
			sb.append(sigmoidalContrast);
		}
		sb.append(']');

		return sb.toString();
	}
}
