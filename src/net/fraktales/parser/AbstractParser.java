package net.fraktales.parser;

import net.fraktales.tools.ConsoleLogger;

public abstract class AbstractParser {

	private final String outputFileName;
	private String inputFileName;

	public AbstractParser(String outputFileName) {
		this.outputFileName = outputFileName;
		inputFileName = null;
	}

	public abstract Filter getFilter();
	public abstract boolean parse(String[] args);

	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void printArgs() {
		ConsoleLogger.getInstance().info("usage:\n" + getFilter().usage);
	}
}
