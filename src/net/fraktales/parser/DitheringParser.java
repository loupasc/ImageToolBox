package net.fraktales.parser;

import net.fraktales.param.DitherQuantization;
import net.fraktales.tools.ConsoleLogger;

public class DitheringParser extends AbstractParser {

	private DitherQuantization quantization;

	public DitheringParser(String outputFileName) {
		super(outputFileName);
		quantization = DitherQuantization.BINARY;
	}

	@Override
	public Filter getFilter() {
		return Filter.DITHER;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 2) {
			setInputFileName(args[1]);
		}
		else if (args.length == 3) {
			try {
				quantization = DitherQuantization.valueOf(args[1].toUpperCase());
			}
			catch (IllegalArgumentException e) {
				ConsoleLogger.getInstance().error("Invalid dither quantization: " + args[1]);
				printArgs();
				return false;
			}
			setInputFileName(args[2]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public DitherQuantization getQuantization() {
		return quantization;
	}

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder("DitheringParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(']');

		return sb.toString();
	}
}