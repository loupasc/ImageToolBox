package net.fraktales.parser;

import net.fraktales.tools.ConsoleLogger;

public class BlockifierParser extends AbstractParser {

	private boolean tinyPatternEnabled;
	private boolean softerRenderingEnabled;

	public BlockifierParser(String outputFileName) {
		super(outputFileName);
		tinyPatternEnabled = false;
		softerRenderingEnabled = false;
	}

	@Override
	public Filter getFilter() {
		return Filter.BLOCKIFIER;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 2) {
			setInputFileName(args[1]);
		}
		else if ((args.length == 3) || (args.length == 4)) {
			for (int i = 1; i < (args.length - 1); i++) {
				if (args[i].compareToIgnoreCase("softer") == 0) {
					softerRenderingEnabled = true;
				}
				else if (args[i].compareToIgnoreCase("tiny_pattern") == 0) {
					tinyPatternEnabled = true;
				}
				else {
					ConsoleLogger.getInstance().error("Invalid option " + args[i]);
					printArgs();
					return false;
				}
			}
			setInputFileName(args[args.length - 1]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public boolean isTinyPatternEnabled() {
		return tinyPatternEnabled;
	}

	public boolean isSofterRenderingEnabled() {
		return softerRenderingEnabled;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("BlockifierParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", tinyPatternEnabled=");
		sb.append(tinyPatternEnabled);
		sb.append(", softerRenderingEnabled=");
		sb.append(softerRenderingEnabled);
		sb.append(']');

		return sb.toString();
	}
}
