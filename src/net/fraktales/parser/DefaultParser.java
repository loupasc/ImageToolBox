package net.fraktales.parser;

import javax.imageio.ImageIO;

import net.fraktales.tools.ConsoleLogger;

public class DefaultParser extends AbstractParser {

	public DefaultParser() {
		super(null);
	}

	@Override
	public Filter getFilter() {
		return Filter.NULL;
	}

	@Override
	public boolean parse(String[] args) {
		printArgs();
		return false;
	}

	@Override
	public void printArgs() {
		StringBuilder commandHelp = new StringBuilder("Supported image formats: [ ");
		for (String imageFormat : ImageIO.getReaderFileSuffixes()) {
			commandHelp.append(imageFormat);
			commandHelp.append(' ');
		}
		commandHelp.append("]\nImageToolBox\t");
		commandHelp.append(Filter.BW_LEVELS.usage);
		commandHelp.append("\n\t\t" + Filter.BLOCKIFIER.usage);
		commandHelp.append("\n\t\t" + Filter.BLUR.usage);
		commandHelp.append("\n\t\t" + Filter.COLORIZER.usage);
		commandHelp.append("\n\t\t" + Filter.GRADIENT_MAPPER.usage);
		commandHelp.append("\n\t\t" + Filter.HALFTONING.usage);
		commandHelp.append("\n\t\t" + Filter.DITHER.usage);
		commandHelp.append("\n\t\t" + Filter.LEVELS_AUTO.usage);
		commandHelp.append("\n\t\t" + Filter.MATCHER.usage);
		commandHelp.append("\n\t\t" + Filter.MIXING.usage);
		commandHelp.append("\n\t\t" + Filter.SHARPEN.usage);
		commandHelp.append("\n\t\t" + Filter.SINC.usage);
		commandHelp.append("\n\t\t" + Filter.SIGMOIDAL_CONTRAST.usage);
		commandHelp.append("\n\t\t" + Filter.UPSCALER2X.usage);

		ConsoleLogger.getInstance().info(commandHelp.toString());
	}
}
