package net.fraktales.parser;

public class ContrastMatcherParser extends AbstractParser {

	private String referenceFileName;

	public ContrastMatcherParser(String outputFile) {
		super(outputFile);
		referenceFileName = null;
	}

	@Override
	public Filter getFilter() {
		return Filter.MATCHER;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			referenceFileName = args[1];
			setInputFileName(args[2]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public String getReferenceFileName() {
		return referenceFileName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ContrastMatcherParser [referenceFileName=");
		sb.append(referenceFileName);
		sb.append(", inputFileName=");
		sb.append(getInputFileName());
		sb.append(']');

		return sb.toString();
	}
}
