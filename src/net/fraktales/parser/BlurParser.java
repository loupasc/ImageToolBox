package net.fraktales.parser;

import net.fraktales.processor.util.GaussianHvsModel.Preset;
import net.fraktales.tools.ConsoleLogger;

public class BlurParser extends AbstractParser {

	private static final Preset[] BLUR_SETTINGS = Preset.values();
	private Preset blurSettings;

	public BlurParser(String outputFile) {
		super(outputFile);
		blurSettings = null;
	}

	@Override
	public Filter getFilter() {
		return Filter.BLUR;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			try {
				short radius = Short.parseShort(args[1]);
				for (Preset settings : BLUR_SETTINGS) {
					if (settings.radius == radius) {
						blurSettings = settings;
						break;
					}
				}
				if (blurSettings == null) {
					ConsoleLogger.getInstance().error(args[1] + " <- Value outside range");
				}
			}
			catch (NumberFormatException e) {
				ConsoleLogger.getInstance().error(args[1] + " <- Integer expected for blur radius");
			}
			if (blurSettings == null) {
				printArgs();
				return false;
			}
			setInputFileName(args[2]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public Preset getBlurSettings() {
		return blurSettings;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("BlurParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", blurSettings=");
		sb.append(blurSettings);
		sb.append(']');

		return sb.toString();
	}
}
