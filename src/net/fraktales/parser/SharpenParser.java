package net.fraktales.parser;

import net.fraktales.tools.ConsoleLogger;

public class SharpenParser extends AbstractParser {

	private static short[] AMOUNT = {2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233};
	private int strength;
	private boolean lumaOnlyEnabled;

	public SharpenParser(String outputFile) {
		super(outputFile);
		lumaOnlyEnabled = false;
	}

	@Override
	public Filter getFilter() {
		return Filter.SHARPEN;
	}

	@Override
	public boolean parse(String[] args) {
		if ((args.length == 3) || (args.length == 4)) {
			int sharpenParam = 0;
			try {
				sharpenParam = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException e) {
				ConsoleLogger.getInstance().error(args[1] + " <- Integer expected for sharpen strength");
				printArgs();
				return false;
			}
			if ((sharpenParam >= 0) && (sharpenParam < AMOUNT.length)) {
				strength = AMOUNT[sharpenParam];
			}
			else {
				ConsoleLogger.getInstance().error(args[1] + " <- Value outside range");
				printArgs();
				return false;
			}
			if ((args.length == 4)) {
				if (args[2].compareToIgnoreCase("luma_only") == 0) {
					lumaOnlyEnabled = true;
				}
				else {
					ConsoleLogger.getInstance().error("Invalid option " + args[2]);
					printArgs();
					return false;
				}
			}
			setInputFileName(args[args.length - 1]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public int getStrength() {
		return strength;
	}

	public boolean isLumaOnlyEnabled() {
		return lumaOnlyEnabled;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("SharpenParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", strength=");
		sb.append(strength);
		sb.append(", lumaOnlyEnabled=");
		sb.append(lumaOnlyEnabled);
		sb.append(']');

		return sb.toString();
	}
}
