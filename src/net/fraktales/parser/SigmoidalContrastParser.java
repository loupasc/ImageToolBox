package net.fraktales.parser;

import net.fraktales.tools.ConsoleLogger;

public class SigmoidalContrastParser extends AbstractParser {

	private static final double PARSE_ERROR = -1.0;
	private double sigmoidalContrast;

	public SigmoidalContrastParser(String outputFileName) {
		super(outputFileName);
		sigmoidalContrast = PARSE_ERROR;
	}

	private static double parseSigmoidalConstrast(String argValue) {
		double result = PARSE_ERROR;
		try {
			result = Double.parseDouble(argValue);
			if (result <= 0) {
				ConsoleLogger.getInstance().error(argValue + " <- Positive value expected for Sigmoidal constrast");
				return PARSE_ERROR;
			}
		}
		catch (NumberFormatException e) {
			ConsoleLogger.getInstance().error(argValue + " <- Decimal expected for Sigmoidal constrast");
		}

		return result;
	}

	@Override
	public Filter getFilter() {
		return Filter.SIGMOIDAL_CONTRAST;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			sigmoidalContrast = parseSigmoidalConstrast(args[1]);
			if (sigmoidalContrast < 0) {
				// PARSE_ERROR
				printArgs();
				return false;
			}
			setInputFileName(args[2]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public double getSigmoidalContrast() {
		return sigmoidalContrast;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("SigmoidalContrastParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", sigmoidalContrast=");
		sb.append(sigmoidalContrast);
		sb.append(']');

		return sb.toString();
	}
}
