package net.fraktales.parser;

import net.fraktales.param.KernelShape;
import net.fraktales.tools.ConsoleLogger;

public class SincParser extends AbstractParser {

	private KernelShape kernelShape;
	private int cutoff;
	private int radius;
	private int contrast;

	public SincParser(String outputFileName) {
		super(outputFileName);
		kernelShape = null;
		cutoff = 2;
		radius = 8;
		contrast = 5;
	}

	private static int parseInt(String argName, String argValue) {
		int result = -1;
		try {
			result = Integer.parseInt(argValue);
		}
		catch (NumberFormatException e) {
			ConsoleLogger.getInstance().error(argValue + " <- Integer expected for " + argName);
		}

		return result;
	}

	@Override
	public Filter getFilter() {
		return Filter.SINC;
	}

	@Override
	public boolean parse(String[] args) {
		if ((args.length == 5) || (args.length == 6)) {
			// kernel shape
			try {
				kernelShape = KernelShape.valueOf(args[1].toUpperCase());
			}
			catch (IllegalArgumentException e) {
				ConsoleLogger.getInstance().error("Invalid kernel shape: " + args[1]);
				printArgs();
				return false;
			}
			// filter cutoff
			int paramValue = parseInt("filter cutoff", args[2]);
			if ((paramValue >= 1) && (paramValue <= 4)) {
				cutoff = paramValue;
			}
			else {
				printArgs();
				return false;
			}
			// kernel radius
			paramValue = parseInt("kernel radius", args[3]);
			if ((paramValue >= 8) && (paramValue <= 100)) {
				radius = paramValue;
			}
			else {
				printArgs();
				return false;
			}
			// global contrast
			if (args.length == 6) {
				paramValue = parseInt("global contrast", args[4]);
				if ((paramValue >= 0) && (paramValue <= 10)) {
					contrast = paramValue;
				}
				else {
					printArgs();
					return false;
				}
			}
			setInputFileName(args[args.length - 1]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public KernelShape getKernelShape() {
		return kernelShape;
	}

	public int getCutoff() {
		return cutoff;
	}

	public int getRadius() {
		return radius;
	}

	public int getContrast() {
		return contrast;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("SincParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", kernelShape=");
		sb.append(kernelShape);
		sb.append(", cutoff=");
		sb.append(cutoff);
		sb.append(", radius=");
		sb.append(radius);
		sb.append(", contrast=");
		sb.append(contrast);
		sb.append(']');

		return sb.toString();
	}
}
