package net.fraktales.parser;

public class ColorizerParser extends AbstractParser {

	private String coloredImageFileName;

	public ColorizerParser(String outputFile) {
		super(outputFile);
		coloredImageFileName = null;
	}

	@Override
	public Filter getFilter() {
		return Filter.COLORIZER;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			setInputFileName(args[1]);
			coloredImageFileName = args[2];
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public String getColoredImageFileName() {
		return coloredImageFileName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ColorizerParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", coloredImageFileName=");
		sb.append(coloredImageFileName);
		sb.append(']');

		return sb.toString();
	}
}
