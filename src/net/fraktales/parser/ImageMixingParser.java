package net.fraktales.parser;

import net.fraktales.param.MaskParameter;

public class ImageMixingParser extends AbstractParser {

	private String secondFileName;
	private MaskParameter maskParameter;

	public ImageMixingParser(String outputFile) {
		super(outputFile);
		secondFileName = null;
		maskParameter = null;
	}

	@Override
	public Filter getFilter() {
		return Filter.MIXING;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 3) {
			setInputFileName(args[1]);
			secondFileName = args[2];
			maskParameter = new MaskParameter();
		}
		else if (args.length == 4) {
			setInputFileName(args[1]);
			secondFileName = args[2];
			try {
				int opacity = Integer.parseInt(args[3]);
				maskParameter = new MaskParameter(opacity);
			}
			catch (NumberFormatException e) {
				maskParameter = new MaskParameter(args[3]);
			}
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public String getSecondFileName() {
		return secondFileName;
	}

	public MaskParameter getMaskParameter() {
		return maskParameter;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ImageMixingParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", secondFileName=");
		sb.append(secondFileName);
		sb.append(", maskParameter=");
		sb.append(maskParameter);
		sb.append(']');

		return sb.toString();
	}
}
