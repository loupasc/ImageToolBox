package net.fraktales.parser;

import net.fraktales.tools.ConsoleLogger;

public class UpscalerParser extends AbstractParser {

	private boolean addSharpness;

	public UpscalerParser(String outputFileName) {
		super(outputFileName);
		addSharpness = false;
	}

	@Override
	public Filter getFilter() {
		return Filter.UPSCALER2X;
	}

	@Override
	public boolean parse(String[] args) {
		if ((args.length == 2) || (args.length == 3)) {
			if (args.length == 3) {
				if (args[1].compareToIgnoreCase("-sharp") == 0) {
					addSharpness = true;
				}
				else {
					ConsoleLogger.getInstance().error("Invalid option " + args[2]);
					printArgs();
					return false;
				}
			}
			setInputFileName(args[args.length - 1]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public boolean isAddSharpness() {
		return addSharpness;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("UpscalerParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", addSharpness=");
		sb.append(addSharpness);
		sb.append(']');

		return sb.toString();
	}
}
