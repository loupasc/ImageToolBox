package net.fraktales.parser;

import net.fraktales.param.DitherQuantization;
import net.fraktales.param.OrderedDitherArg;
import net.fraktales.tools.ConsoleLogger;

public class HalftoningParser extends AbstractParser {

	private OrderedDitherArg orderedDitherArg;
	private DitherQuantization quantization;

	public HalftoningParser(String outputFileName) {
		super(outputFileName);
		orderedDitherArg = OrderedDitherArg.DEFAULT;
		quantization = DitherQuantization.BINARY;
	}

	@Override
	public Filter getFilter() {
		return Filter.HALFTONING;
	}

	@Override
	public boolean parse(String[] args) {
		if (args.length == 2) {
			setInputFileName(args[1]);
		}
		else if (args.length == 3) {
			boolean isValidDitherArg = true;
			try {
				orderedDitherArg = OrderedDitherArg.valueOf(args[1].toUpperCase());
			}
			catch (IllegalArgumentException e) {
				isValidDitherArg = false;
			}
			if (!isValidDitherArg) {
				//
				// Try to read quantization parameter
				//
				try {
					quantization = DitherQuantization.valueOf(args[1].toUpperCase());
				}
				catch (IllegalArgumentException e) {
					//
					// Neither method nor quantization are valid
					// Method first expected
					//
					ConsoleLogger.getInstance().error("Invalid dither method: " + args[1]);
					printArgs();
					return false;
				}
			}
			setInputFileName(args[2]);
		}
		else if (args.length == 4) {
			try {
				orderedDitherArg = OrderedDitherArg.valueOf(args[1].toUpperCase());
			}
			catch (IllegalArgumentException e) {
				ConsoleLogger.getInstance().error("Invalid dither method: " + args[1]);
				printArgs();
				return false;
			}

			try {
				quantization = DitherQuantization.valueOf(args[2].toUpperCase());
			}
			catch (IllegalArgumentException e) {
				ConsoleLogger.getInstance().error("Invalid dither quantization: " + args[2]);
				printArgs();
				return false;
			}
			setInputFileName(args[3]);
		}
		else {
			printArgs();
			return false;
		}

		return true;
	}

	public OrderedDitherArg getOrderedDitherArg() {
		return orderedDitherArg;
	}

	public DitherQuantization getQuantization() {
		return quantization;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("HalftoningParser [inputFileName=");
		sb.append(getInputFileName());
		sb.append(", orderedDitherArg=");
		sb.append(orderedDitherArg);
		sb.append(", quantization=");
		sb.append(quantization);
		sb.append(']');

		return sb.toString();
	}
}
