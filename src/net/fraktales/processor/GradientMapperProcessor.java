package net.fraktales.processor;

import java.util.Arrays;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.GradientMapperColor;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.ProgressLog;

public class GradientMapperProcessor extends AbstractProcessor {

	private static final int RANGE = 85;

	private final int[] gradient;

	public GradientMapperProcessor(ProcessorListener processorListener, GradientMapperColor[] colors, boolean isSofterRendering) {
		super(processorListener);
		gradient = createGradient(colors, isSofterRendering);
	}

	private static int[] createGammaArray(boolean isSofterRendering) {
		int[] gammaArray = new int[RANGE];

		if (isSofterRendering) {
			for (int i = 0; i < RANGE; i++) {
				gammaArray[i] = 3 * i;
			}
		}
		else {
			for (int i = 0; i < 21; i++) {
				gammaArray[i] = i;
			}
			gammaArray[21] = 22;
			gammaArray[22] = 25;
			for (int i = 23; i < 43; i++) {
				gammaArray[i] = 5 * i - 86;
			}
			for (int i = 43; i < RANGE; i++) {
				gammaArray[i] = 255 - gammaArray[RANGE - i];
			}
		}

		return gammaArray;
	}

	private static int getRgbColor(GradientMapperColor c1, GradientMapperColor c2, int coef) {
		int r = (c1.r * (255 - coef) + c2.r * coef) / 255;
		int g = (c1.g * (255 - coef) + c2.g * coef) / 255;
		int b = (c1.b * (255 - coef) + c2.b * coef) / 255;

		return BufferedImageUtils.packRgbBytes(r, g, b);
	}

	private static int[] createGradient(GradientMapperColor[] colors, boolean isSofterRendering) {
		int[] gradient = new int[256];
		int[] gammaArray = createGammaArray(isSofterRendering);

		if (colors.length < 4) {
			throw new IllegalArgumentException("Invalid input: " + Arrays.toString(colors));
		}

		for (int i = 0; i < RANGE; i++) {
			gradient[i] = getRgbColor(colors[0], colors[1], gammaArray[i]);
		}
		for (int i = 0; i < RANGE; i++) {
			gradient[RANGE + i] = getRgbColor(colors[1], colors[2], gammaArray[i]);
		}
		for (int i = 0; i < RANGE; i++) {
			gradient[2 * RANGE + i] = getRgbColor(colors[2], colors[3], gammaArray[i]);
		}
		gradient[3 * RANGE] = getRgbColor(colors[3], colors[3], 0);

		return gradient;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		ProgressLog gradientMappingProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			gradientMappingProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int luma8bit = ColorTransformation.rgbToGray(BufferedImageUtils.getRed(pixelColor),
						BufferedImageUtils.getGreen(pixelColor),
						BufferedImageUtils.getBlue(pixelColor),
						(i ^ j) & 0x1);
				canvas.setRGB(i, j, gradient[luma8bit]);
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
