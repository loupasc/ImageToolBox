package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.processor.util.GaussianHvsModel;
import net.fraktales.processor.util.GaussianHvsModel.Preset;
import net.fraktales.processor.util.KernelFilter;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.ProgressLog;

public class DitheringProcessor extends AbstractProcessor {

	//
	// Non-rectangular dither tile (it's why coefficients appear several times)
	// [   5 3   ]
	// [ 7 1 6 4 ]
	// [   8 2   ]
	// Coefficients are stored in a square matrix for performance purpose.
	//
	private static final int[] DITHER_MAP = new int[] {
		4, 5, 3, 8, 2, 7, 1, 6,
		7, 1, 6, 4, 5, 3, 8, 2,
		3, 8, 2, 7, 1, 6, 4, 5,
		6, 4, 5, 3, 8, 2, 7, 1,
		2, 7, 1, 6, 4, 5, 3, 8,
		5, 3, 8, 2, 7, 1, 6, 4,
		1, 6, 4, 5, 3, 8, 2, 7,
		8, 2, 7, 1, 6, 4, 5, 3
	};

	//
	// Progress log heuristic
	//
	private static final int ITERATION_RATIO = 3628800; // factorial(10)

	//
	// Numerical value corresponding to the maximum deep gray value
	//
	private static final int WHITE = ColorTransformation.rgbToDeepGray(255, 255, 255);
	
	//
	// 9 pseudo-colors
	//
	private static final int PSEUDO_COLOR_SCALE = WHITE / 9;

	public DitheringProcessor(ProcessorListener processorListener) {
		super(processorListener);
	}

	private static int getProgressionFromUpdatedPixelCount(int pixelUpdatedCount) {
		int progressIndex = 1;
		int threshold = ITERATION_RATIO;
		int thresholdDivisor = 10;
		while (pixelUpdatedCount < threshold) {
			threshold = threshold / thresholdDivisor;
			thresholdDivisor = thresholdDivisor - 1;
			progressIndex = progressIndex + 1;
		}

		return progressIndex;
	}

	private static int tryToTogglePixel(Canvas image, int x, int y, Canvas convolvedImage, KernelFilter kernel) {
		int originalConvolvedPixel = convolvedImage.getRGB(x, y);
		int diff = Math.abs(kernel.convolvePixel(x, y, image) - originalConvolvedPixel);
		// try
		int currentPixelValue = image.getRGB(x, y);
		image.setRGB(x, y, (currentPixelValue == 0) ? WHITE : 0);
		if (Math.abs(kernel.convolvePixel(x, y, image) - originalConvolvedPixel) < diff)  {
			return 1;
		}
		// else restore
		image.setRGB(x, y, currentPixelValue);
		return 0;
	}

	private static int getNextPixelValue(Canvas image, int x , int y, boolean horizontalSwap) {
		return horizontalSwap ? image.getRGB(x + 1, y) : image.getRGB(x, y + 1);
	}

	private static int getNextConvolvedPixel(Canvas image, int x , int y, boolean horizontalSwap, KernelFilter kernel) {
		return horizontalSwap ? kernel.convolvePixel(x + 1, y, image) : kernel.convolvePixel(x, y + 1, image);
	}

	private static int tryToSwapPixel(Canvas image, boolean horizontalSwap, int x, int y, Canvas convolvedImage, KernelFilter kernel) {
		int currentPixelValue = image.getRGB(x, y);
		int nextPixelValue = getNextPixelValue(image, x, y, horizontalSwap);
		if (currentPixelValue != nextPixelValue) {
			int currentConvolvedPixel = convolvedImage.getRGB(x, y);
			int nextConvolvedPixel = getNextPixelValue(convolvedImage, x, y, horizontalSwap);
			int currentDiff = Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel);
			int nextDiff = Math.abs(getNextConvolvedPixel(image, x, y, horizontalSwap, kernel) - nextConvolvedPixel);
			// try
			image.setRGB(x, y, nextPixelValue);
			if (horizontalSwap) {
				image.setRGB(x + 1, y, currentPixelValue);
			}
			else {
				image.setRGB(x, y + 1, currentPixelValue);
			}
			if ((Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel) < currentDiff)
					&& (Math.abs(getNextConvolvedPixel(image, x, y, horizontalSwap, kernel) - nextConvolvedPixel) < nextDiff)) {
				return 1;
			}
			// else restore
			image.setRGB(x, y, currentPixelValue);
			if (horizontalSwap) {
				image.setRGB(x + 1, y, nextPixelValue);
			}
			else {
				image.setRGB(x, y + 1, nextPixelValue);
			}
		}
		return 0;
	}

	private static Canvas createConvolvedImage(Canvas image, KernelFilter kernel) {
		Canvas convolvedImage = image.clone();

		for (int j = 4; j < image.height - 5; j++) {
			for (int i = 4; i < image.width - 5; i++) {
				convolvedImage.setRGB(i, j, kernel.convolvePixel(i, j, image));
			}
		}

		return convolvedImage;
	}

	private static int getBinary(int luma16bit, int x, int y) {
		int ditherPattern = DITHER_MAP[((y & 0x7) << 3) | (x & 0x7)];
		return (PSEUDO_COLOR_SCALE * ditherPattern + luma16bit) >> 16;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);
		//
		// convert to 16-bit luma
		//
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int luma16bit = ColorTransformation.rgbToDeepGray(BufferedImageUtils.getRed(pixelColor), 
						BufferedImageUtils.getGreen(pixelColor),
						BufferedImageUtils.getBlue(pixelColor));
				canvas.setRGB(i, j, luma16bit);
			}
		}

		ProgressLog ditheringProgress = new ProgressLog(10);
		KernelFilter kernel = new KernelFilter(GaussianHvsModel.createMatrix(Preset.M9x9));

		Canvas convolvedImage = createConvolvedImage(canvas, kernel);

		//
		// First step - Posterization of the image to process
		// Extreme border values are final and they are half-toned
		// using non-rectangular pattern.
		// Center values will be brighten so they are truncated.
		//
		ditheringProgress.display(0);
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma16bit = canvas.getRGB(i, j);
				if ((i >= 4) && (i < canvas.width - 4) && (j >= 4) && (j < canvas.height - 4)) {
					canvas.setRGB(i, j, (luma16bit == WHITE) ? WHITE : 0);
				}
				else {
					canvas.setRGB(i, j, WHITE * getBinary(luma16bit, i, j));
				}
			}
		}

		int progressIndex = 1;
		ditheringProgress.display(progressIndex);

		//
		// First pass - Toggle pixels
		//
		int pixelUpdatedCount = Integer.MAX_VALUE;
		while (pixelUpdatedCount > 0) {
			// non-linear progression
			int newProgress = getProgressionFromUpdatedPixelCount(pixelUpdatedCount);
			while (progressIndex < newProgress) {
				progressIndex = progressIndex + 1;
				ditheringProgress.display(progressIndex);
			}
			// processing
			pixelUpdatedCount = 0;
			for (int j = 4; j < canvas.height - 4; j++) {
				for (int i = 4; i < canvas.width - 4; i++) {
					pixelUpdatedCount = pixelUpdatedCount
							+ tryToTogglePixel(canvas,
								i, j,
								convolvedImage,
								kernel);
				}
			}
		}

		while (progressIndex < 8) {
			progressIndex = progressIndex + 1;
			ditheringProgress.display(progressIndex);
		}

		//
		// Second pass - Swap pixels
		//
		pixelUpdatedCount = Integer.MAX_VALUE;
		while (pixelUpdatedCount > 0) {
			pixelUpdatedCount = 0;
			for (int j = 4; j < canvas.height - 4; j++) {
				for (int i = 4; i < canvas.width - 5; i++) {
					pixelUpdatedCount = pixelUpdatedCount
							+ tryToSwapPixel(canvas, true,
								i, j,
								convolvedImage,
								kernel);
				}
			}
		}

		ditheringProgress.display(9);
		pixelUpdatedCount = Integer.MAX_VALUE;
		while (pixelUpdatedCount > 0) {
			pixelUpdatedCount = 0;
			for (int j = 4; j < canvas.height - 5; j++) {
				for (int i = 4; i < canvas.width - 4; i++) {
					pixelUpdatedCount = pixelUpdatedCount
							+ tryToSwapPixel(canvas, false,
								i, j,
								convolvedImage,
								kernel);
				}
			}
		}

		ditheringProgress.display(10);

		//
		// convert to 8-bit RGB
		//
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma16bit = canvas.getRGB(i, j);
				canvas.setRGB(i, j, (luma16bit == WHITE) ? 0xFFFFFF : 0x000000);
			}
		}
		processorListener.exportCanvasIndexedColor();
	}
}