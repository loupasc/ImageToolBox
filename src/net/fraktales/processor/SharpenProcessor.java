package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.GaussianUnsharpMask;
import net.fraktales.tools.ProgressLog;

public class SharpenProcessor extends AbstractProcessor {

	private final GaussianUnsharpMask sharpen;
	private final boolean lumaOnly;

	public SharpenProcessor(ProcessorListener processorListener, int strength, boolean lumaOnly) {
		super(processorListener);
		sharpen = new GaussianUnsharpMask(strength);
		this.lumaOnly = lumaOnly;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		Canvas input = canvas.clone();
		ProgressLog.loadingImage("input image", input.width, input.height);

		ProgressLog sharpenProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			sharpenProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = (lumaOnly ? sharpen.convolveYUV(input, i, j)
						: sharpen.convolveRGB(input, i, j));
				canvas.setRGB(i, j, pixelColor);
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
