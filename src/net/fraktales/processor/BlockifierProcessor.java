package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.CubicInterpolator;
import net.fraktales.tools.ProgressLog;

public class BlockifierProcessor extends AbstractProcessor {

	//
	// -- Pattern declaration --
	//

	private static final short[] BLANK_PATTERN = {
		0, 0, 0,
		0, 0, 0,
		0, 0, 0
	};
	private static final short[] FULL_PATTERN = {
		255, 255, 255,
		255, 255, 255,
		255, 255, 255
	};
	private static final short[] UP_LEFT_SQUARE_PATTERN = {
		255, 255, 0,
		255, 255, 0,
		0, 0, 0
	};
	private static final short[] UP_RIGHT_SQUARE_PATTERN = {
		0, 255, 255,
		0, 255, 255,
		0, 0, 0
	};
	private static final short[] DOWN_RIGHT_SQUARE_PATTERN = {
		0, 0, 0,
		0, 255, 255,
		0, 255, 255
	};
	private static final short[] DOWN_LEFT_SQUARE_PATTERN = {
		0, 0, 0,
		255, 255, 0,
		255, 255, 0
	};
	private static final short[] CENTER_DOT_PATTERN = {
		0, 0, 0,
		0, 255, 0,
		0, 0, 0
	};
	private static final short[] UP_DOT_PATTERN = {
		0, 255, 0,
		0, 0, 0,
		0, 0, 0
	};
	private static final short[] RIGHT_DOT_PATTERN = {
		0, 0, 0,
		0, 0, 255,
		0, 0, 0
	};
	// extra pattern
	private static final short[] EXTRA_PATTERN = {
		255, 0, 0,
		255, 0, 0,
		0, 0, 0
	};

	private static final short[][] ARRAY_OF_PATTERNS = {
		BLANK_PATTERN,
		FULL_PATTERN,
		UP_LEFT_SQUARE_PATTERN,
		UP_RIGHT_SQUARE_PATTERN,
		DOWN_RIGHT_SQUARE_PATTERN,
		DOWN_LEFT_SQUARE_PATTERN,
		CENTER_DOT_PATTERN,
		UP_DOT_PATTERN,
		RIGHT_DOT_PATTERN,
		EXTRA_PATTERN
	};

	private final boolean tinyPattern;
	private final int patternDiffCoef;
	private final int weightedDiffCoef;

	public BlockifierProcessor(ProcessorListener processorListener, boolean tinyPattern , boolean isSofterRendering) {
		super(processorListener);
		this.tinyPattern = tinyPattern;
		patternDiffCoef = isSofterRendering ? 51 : 1;
		weightedDiffCoef = isSofterRendering ? 9 : 1;
	}

	private static int toRgbBytes(short luma8bit) {
		return BufferedImageUtils.packRgbBytes(luma8bit, luma8bit, luma8bit);
	}

	private static void copyPixelSquare3x3(Canvas src, int x, int y, short[] dest) {
		dest[0] = (short) src.getRGB(x, y);
		dest[1] = (short) src.safeGetRGB(x + 1, y);
		dest[2] = (short) src.safeGetRGB(x + 2, y);
		dest[3] = (short) src.safeGetRGB(x, y + 1);
		dest[4] = (short) src.safeGetRGB(x + 1, y + 1);
		dest[5] = (short) src.safeGetRGB(x + 2, y + 1);
		dest[6] = (short) src.safeGetRGB(x, y + 2);
		dest[7] = (short) src.safeGetRGB(x + 1, y + 2);
		dest[8] = (short) src.safeGetRGB(x + 2, y + 2);
	}

	private static void copyPattern(short[] selectedPattern, Canvas image, int x, int y) {
		image.setRGB(x, y, toRgbBytes(selectedPattern[0]));
		image.safeSetRGB(x + 1, y, toRgbBytes(selectedPattern[1]));
		image.safeSetRGB(x + 2, y, toRgbBytes(selectedPattern[2]));
		image.safeSetRGB(x, y + 1, toRgbBytes(selectedPattern[3]));
		image.safeSetRGB(x + 1, y + 1, toRgbBytes(selectedPattern[4]));
		image.safeSetRGB(x + 2, y + 1, toRgbBytes(selectedPattern[5]));
		image.safeSetRGB(x, y + 2, toRgbBytes(selectedPattern[6]));
		image.safeSetRGB(x + 1, y + 2, toRgbBytes(selectedPattern[7]));
		image.safeSetRGB(x + 2, y + 2, toRgbBytes(selectedPattern[8]));
	}

	//   [a][b][c][0]
	//   [d][e][f][1]
	//   [g][h][i][2]
	//[3][4][5][6][7]
	private static int[] getPixelsToUpdate(Canvas image, int x, int y) {
		int[] pixelsToUpdate = new int[8];

		pixelsToUpdate[0] = image.safeGetRGB(x + 3, y);
		pixelsToUpdate[1] = image.safeGetRGB(x + 3, y + 1);
		pixelsToUpdate[2] = image.safeGetRGB(x + 3, y + 2);
		pixelsToUpdate[3] = image.safeGetRGB(x - 1, y + 3);
		pixelsToUpdate[4] = image.safeGetRGB(x, y + 3);
		pixelsToUpdate[5] = image.safeGetRGB(x + 1, y + 3);
		pixelsToUpdate[6] = image.safeGetRGB(x + 2, y + 3);
		pixelsToUpdate[7] = image.safeGetRGB(x + 3, y + 3);

		return pixelsToUpdate;
	}

	private static void updatePixels(Canvas image, int x, int y, int[] pixelsToUpdate) {
		image.safeSetRGB(x + 3, y, pixelsToUpdate[0]);
		image.safeSetRGB(x + 3, y + 1, pixelsToUpdate[1]);
		image.safeSetRGB(x + 3, y + 2, pixelsToUpdate[2]);
		image.safeSetRGB(x - 1, y + 3, pixelsToUpdate[3]);
		image.safeSetRGB(x, y + 3, pixelsToUpdate[4]);
		image.safeSetRGB(x + 1, y + 3, pixelsToUpdate[5]);
		image.safeSetRGB(x + 2, y + 3, pixelsToUpdate[6]);
		image.safeSetRGB(x + 3, y + 3, pixelsToUpdate[7]);
	}

	private static short div64(int x) {
		if (x > 0) return (short) ((x + 32) / 64);
		if (x < 0) return (short) ((x - 32) / 64);
		return 0;
	}

	//
	// !!! pixelsToUpdate is input and output => pixelsToUpdate's content is modified
	//
	private static void diffuseError(short[] pixelSquare3x3, int[] pixelsToUpdate, short[] selectedPattern) {
		//   [a][b][c][0]
		//   [d][e][f][1]
		//   [g][h][i][2]
		//[3][4][5][6][7]
		pixelsToUpdate[0] = 64 * pixelsToUpdate[0];
		pixelsToUpdate[1] = 64 * pixelsToUpdate[1];
		pixelsToUpdate[2] = 64 * pixelsToUpdate[2];
		pixelsToUpdate[3] = 64 * pixelsToUpdate[3];
		pixelsToUpdate[4] = 64 * pixelsToUpdate[4];
		pixelsToUpdate[5] = 64 * pixelsToUpdate[5];
		pixelsToUpdate[6] = 64 * pixelsToUpdate[6];
		pixelsToUpdate[7] = 64 * pixelsToUpdate[7];

		// [a]
		int error = pixelSquare3x3[0] - selectedPattern[0];
		pixelsToUpdate[0] = pixelsToUpdate[0] + 2 * error;
		pixelsToUpdate[1] = pixelsToUpdate[1] + 5 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 6 * error;
		pixelsToUpdate[3] = pixelsToUpdate[3] + 5 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 17 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 17 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 9 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [b]
		error = pixelSquare3x3[1] - selectedPattern[1];
		pixelsToUpdate[0] = pixelsToUpdate[0] + 6 * error;
		pixelsToUpdate[1] = pixelsToUpdate[1] + 9 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 8 * error;
		pixelsToUpdate[3] = pixelsToUpdate[3] + 2 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 11 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 16 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 11 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [c]
		error = pixelSquare3x3[2] - selectedPattern[2];
		pixelsToUpdate[0] = pixelsToUpdate[0] + 20 * error;
		pixelsToUpdate[1] = pixelsToUpdate[1] + 14 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 8 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 3 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 9 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 9 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [d]
		error = pixelSquare3x3[3] - selectedPattern[3];
		pixelsToUpdate[1] = pixelsToUpdate[1] + 2 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 5 * error;
		pixelsToUpdate[3] = pixelsToUpdate[3] + 7 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 23 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 18 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 8 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [e]
		error = pixelSquare3x3[4] - selectedPattern[4];
		pixelsToUpdate[1] = pixelsToUpdate[1] + 6 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 9 * error;
		pixelsToUpdate[3] = pixelsToUpdate[3] + 2 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 12 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 21 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 13 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [f]
		error = pixelSquare3x3[5] - selectedPattern[5];
		pixelsToUpdate[1] = pixelsToUpdate[1] + 20 * error;
		pixelsToUpdate[2] = pixelsToUpdate[2] + 14 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 2 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 11 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 15 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + 2 * error;
		// [g]
		error = pixelSquare3x3[6] - selectedPattern[6];
		pixelsToUpdate[2] = pixelsToUpdate[2] + 2 * error;
		pixelsToUpdate[3] = pixelsToUpdate[3] + 12 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 32 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 14 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 4 * error;
		// [h]
		error = pixelSquare3x3[7] - selectedPattern[7];
		pixelsToUpdate[2] = pixelsToUpdate[2] + 6 * error;
		pixelsToUpdate[4] = pixelsToUpdate[4] + 12 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 32 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 13 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + error;
		// [i]
		error = pixelSquare3x3[8] - selectedPattern[8];
		pixelsToUpdate[2] = pixelsToUpdate[2] + 20 * error;
		pixelsToUpdate[5] = pixelsToUpdate[5] + 12 * error;
		pixelsToUpdate[6] = pixelsToUpdate[6] + 28 * error;
		pixelsToUpdate[7] = pixelsToUpdate[7] + 4 * error;

		// Apply error diffusion
		pixelsToUpdate[0] = div64(pixelsToUpdate[0]);
		pixelsToUpdate[1] = div64(pixelsToUpdate[1]);
		pixelsToUpdate[2] = div64(pixelsToUpdate[2]);
		pixelsToUpdate[3] = div64(pixelsToUpdate[3]);
		pixelsToUpdate[4] = div64(pixelsToUpdate[4]);
		pixelsToUpdate[5] = div64(pixelsToUpdate[5]);
		pixelsToUpdate[6] = div64(pixelsToUpdate[6]);
		pixelsToUpdate[7] = div64(pixelsToUpdate[7]);
	}

	private static short getMiniPixel(Canvas canvas, int x, int y) {
		double[] bicubicResult = CubicInterpolator.createArrayOfPoints();
		double[] arrayOfPoints = CubicInterpolator.createArrayOfPoints();

		for (int i = 0; i < bicubicResult.length; i++) {
			arrayOfPoints[0] = canvas.safeGetRGB(x - 1, y + i - 1);
			arrayOfPoints[1] = canvas.safeGetRGB(x, y + i - 1);
			arrayOfPoints[2] = canvas.safeGetRGB(x + 1, y + i - 1);
			arrayOfPoints[3] = canvas.safeGetRGB(x + 2, y + i - 1);
			bicubicResult[i] = CubicInterpolator.applyCenterInterpolation(arrayOfPoints);
		}

		return (short) Math.round(CubicInterpolator.applyCenterInterpolation(bicubicResult));
	}

	// Weighted comparison (upper left pixel harder to compensate)
	// [ 10 7 5 ]
	// [  7 6 4 ]
	// [  5 4 3 ]
	// 3x3 pixel square
	private int getPatternDiff(short[] pixelSquare3x3, short[] pattern) {
		int patternDiff = Math.abs(pixelSquare3x3[0] - pattern[0]
				+ pixelSquare3x3[1] - pattern[1]
				+ pixelSquare3x3[2] - pattern[2]
				+ pixelSquare3x3[3] - pattern[3]
				+ pixelSquare3x3[4] - pattern[4]
				+ pixelSquare3x3[5] - pattern[5]
				+ pixelSquare3x3[6] - pattern[6]
				+ pixelSquare3x3[7] - pattern[7]
				+ pixelSquare3x3[8] - pattern[8]);
		int weightedError = (10 * Math.abs(pixelSquare3x3[0] - pattern[0])
				+ 7 * Math.abs(pixelSquare3x3[1] - pattern[1])
				+ 5 * Math.abs(pixelSquare3x3[2] - pattern[2])
				+ 7 * Math.abs(pixelSquare3x3[3] - pattern[3])
				+ 6 * Math.abs(pixelSquare3x3[4] - pattern[4])
				+ 4 * Math.abs(pixelSquare3x3[5] - pattern[5])
				+ 5 * Math.abs(pixelSquare3x3[6] - pattern[6])
				+ 4 * Math.abs(pixelSquare3x3[7] - pattern[7])
				+ 3 * Math.abs(pixelSquare3x3[8] - pattern[8]));

		return patternDiffCoef * patternDiff + weightedDiffCoef * weightedError;
	}

	private void blockifyCanvas(Canvas canvas) {
		int nbHorizontalSubBlocks = (canvas.width + 2) / 3;
		int nbVerticalSubBlocks = (canvas.height + 2) / 3;

		ProgressLog blockifierProgess = new ProgressLog(nbVerticalSubBlocks);
		for (int j = 0; j < nbVerticalSubBlocks; j++) {
			blockifierProgess.display(j);
			int arrayOfPatternLength = 0;
			short[] pixelSquare3x3 = new short[9];
			short[] closestPattern = null;
			int posY = 3 * j;
			for (int i = 0; i < nbHorizontalSubBlocks; i++) {
				int posX = 3 * i;
				if (closestPattern == UP_RIGHT_SQUARE_PATTERN) {
					// include extra pattern
					arrayOfPatternLength = ARRAY_OF_PATTERNS.length;
				}
				else {
					arrayOfPatternLength = ARRAY_OF_PATTERNS.length - 1;
				}
				copyPixelSquare3x3(canvas, posX, posY, pixelSquare3x3);
				closestPattern = ARRAY_OF_PATTERNS[0];
				int diffMin = getPatternDiff(pixelSquare3x3, closestPattern);
				for (int p = 1; p < arrayOfPatternLength; p++) {
					int diff = getPatternDiff(pixelSquare3x3, ARRAY_OF_PATTERNS[p]);
					if (diff < diffMin) {
						closestPattern = ARRAY_OF_PATTERNS[p];
						diffMin = diff;
					}
				}

				int[] pixelsToUpdate = getPixelsToUpdate(canvas, posX, posY);
				diffuseError(pixelSquare3x3, pixelsToUpdate, closestPattern);
				updatePixels(canvas, posX, posY, pixelsToUpdate);
				copyPattern(closestPattern, canvas, posX, posY);
			}
		}
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		canvas.rgbToGray();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		if (tinyPattern) {
			blockifyCanvas(canvas);
		}
		else {
			// Creates an empty miniature canvas
			Canvas miniCanvas = new Canvas((canvas.width + 1) / 2, (canvas.height + 1) / 2);
			// Fill canvas
			for (int j = 0; j < miniCanvas.height; j++) {
				for (int i = 0; i < miniCanvas.width; i++) {
					miniCanvas.setRGB(i, j, getMiniPixel(canvas, 2 * i,  2 * j));
				}
			}
			blockifyCanvas(miniCanvas);
			// Makes square patterns larger
			for (int j = 0; j < miniCanvas.height; j++) {
				for (int i = 0; i < miniCanvas.width; i++) {
					int currentPixel = miniCanvas.getRGB(i, j);
					int nextRightPixel = miniCanvas.safeGetRGB(i + 1, j);
					int nextBottomPixel = miniCanvas.safeGetRGB(i, j + 1);
					int nextRightBottomPixel = miniCanvas.safeGetRGB(i + 1, j + 1);

					canvas.setRGB(2 * i, 2 * j, currentPixel);
					if ((currentPixel == 0) && (nextRightPixel == 0)) {
						canvas.safeSetRGB(2 * i + 1, 2 * j, 0);
					}
					else {
						canvas.safeSetRGB(2 * i + 1, 2 * j, toRgbBytes((short) 255));
					}
					if ((currentPixel == 0) && (nextBottomPixel == 0)) {
						canvas.safeSetRGB(2 * i, 2 * j + 1, 0);
						if ((nextRightPixel == 0) && (nextRightBottomPixel == 0)) {
							canvas.safeSetRGB(2 * i + 1, 2 * j + 1, 0);
						}
						else {
							canvas.safeSetRGB(2 * i + 1, 2 * j + 1, toRgbBytes((short) 255));
						}
					}
					else {
						canvas.safeSetRGB(2 * i, 2 * j + 1, toRgbBytes((short) 255));
						canvas.safeSetRGB(2 * i + 1, 2 * j + 1, toRgbBytes((short) 255));
					}
				}
			}
		}

		processorListener.exportCanvasIndexedColor();
	}
}
