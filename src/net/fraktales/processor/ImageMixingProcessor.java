package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ConsoleLogger;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.ProgressLog;

public class ImageMixingProcessor extends AbstractProcessor {

	private final Canvas secondImage;
	private final Canvas maskImage;
	private final int opacity;
	private final DitherOperator ditherOperator;

	public ImageMixingProcessor(ProcessorListener processorListener, Canvas secondImage, Canvas maskImage) {
		this(processorListener, secondImage, maskImage, 0, new DitherOperator(65025));//255*255
	}

	public ImageMixingProcessor(ProcessorListener processorListener, Canvas secondImage) {
		this(processorListener, secondImage, null, Integer.MIN_VALUE, new DitherOperator(510));//255+255
	}

	public ImageMixingProcessor(ProcessorListener processorListener, Canvas secondImage, int opacity) {
		this(processorListener, secondImage, null, opacity, new DitherOperator(65025));//255*255
	}

	private ImageMixingProcessor(ProcessorListener processorListener, Canvas secondImage, Canvas maskImage, int opacity, DitherOperator ditherOperator) {
		super(processorListener);
		this.secondImage = secondImage;
		this.maskImage = maskImage;
		this.opacity = opacity;
		this.ditherOperator = ditherOperator;
	}

	private void processLayerMaskMixing() {
		Canvas canvas = processorListener.getCanvas();
		if ((canvas.width == secondImage.width) && (secondImage.width == maskImage.width)
				&& (canvas.height == secondImage.height) && (secondImage.height == maskImage.height)) {
			int width = canvas.width;
			int height = canvas.height;
			ProgressLog.loadingImage("first image", width, height);
			ProgressLog.loadingImage("second image", width, height);
			ProgressLog.loadingImage("mask image", width, height);

			ProgressLog mixingProgess = new ProgressLog(height);
			for (int j = 0; j < height; j++) {
				mixingProgess.display(j);
				for (int i = 0; i < width; i++) {
					int firstImagePixel = canvas.getRGB(i, j);
					int secondImagePixel = secondImage.getRGB(i, j);
					int pixelMask = maskImage.getRGB(i, j);
					int redMask = BufferedImageUtils.getRed(pixelMask);
					int greenMask = BufferedImageUtils.getGreen(pixelMask);
					int blueMask = BufferedImageUtils.getBlue(pixelMask);

					int r = ditherOperator.apply8bitsDithering(BufferedImageUtils.getRed(firstImagePixel) * redMask + BufferedImageUtils.getRed(secondImagePixel) * (255 - redMask), i, j);
					int g = ditherOperator.apply8bitsDithering(BufferedImageUtils.getGreen(firstImagePixel) * greenMask + BufferedImageUtils.getGreen(secondImagePixel) * (255 - greenMask), i, j);
					int b = ditherOperator.apply8bitsDithering(BufferedImageUtils.getBlue(firstImagePixel) * blueMask + BufferedImageUtils.getBlue(secondImagePixel) * (255 - blueMask), i, j);

					canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
				}
			}

			processorListener.exportCanvasTrueColor();
		}
		else {
			ConsoleLogger.getInstance().error("Image sizes mismatch, cannot mix images");
		}
	}

	private void processSimpleMixing() {
		Canvas canvas = processorListener.getCanvas();
		if ((canvas.width == secondImage.width) && (canvas.height == secondImage.height)) {
			int width = canvas.width;
			int height = canvas.height;
			ProgressLog.loadingImage("first image", width, height);
			ProgressLog.loadingImage("second image", width, height);

			ProgressLog mixingProgess = new ProgressLog(height);
			for (int j = 0; j < height; j++) {
				mixingProgess.display(j);
				for (int i = 0; i < width; i++) {
					int firstImagePixel = canvas.getRGB(i, j);
					int secondImagePixel = secondImage.getRGB(i, j);

					int r = ditherOperator.apply8bitsDithering(BufferedImageUtils.getRed(firstImagePixel) + BufferedImageUtils.getRed(secondImagePixel), i, j);
					int g = ditherOperator.apply8bitsDithering(BufferedImageUtils.getGreen(firstImagePixel) + BufferedImageUtils.getGreen(secondImagePixel), i, j);
					int b = ditherOperator.apply8bitsDithering(BufferedImageUtils.getBlue(firstImagePixel) + BufferedImageUtils.getBlue(secondImagePixel), i, j);

					canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
				}
			}

			processorListener.exportCanvasTrueColor();
		}
		else {
			ConsoleLogger.getInstance().error("Image sizes mismatch, cannot mix images");
		}
	}

	private void processOpacityMixing() {
		Canvas canvas = processorListener.getCanvas();
		if ((canvas.width == secondImage.width) && (canvas.height == secondImage.height)) {
			int width = canvas.width;
			int height = canvas.height;
			ProgressLog.loadingImage("first image", width, height);
			ProgressLog.loadingImage("second image", width, height);

			int inverseOpacity = 255 - opacity;

			ProgressLog mixingProgess = new ProgressLog(height);
			for (int j = 0; j < height; j++) {
				mixingProgess.display(j);
				for (int i = 0; i < width; i++) {
					int firstImagePixel = canvas.getRGB(i, j);
					int secondImagePixel = secondImage.getRGB(i, j);
					int r = ditherOperator.apply8bitsDithering(BufferedImageUtils.getRed(firstImagePixel) * opacity + BufferedImageUtils.getRed(secondImagePixel) * inverseOpacity, i, j);
					int g = ditherOperator.apply8bitsDithering(BufferedImageUtils.getGreen(firstImagePixel) * opacity + BufferedImageUtils.getGreen(secondImagePixel) * inverseOpacity, i, j);
					int b = ditherOperator.apply8bitsDithering(BufferedImageUtils.getBlue(firstImagePixel) * opacity + BufferedImageUtils.getBlue(secondImagePixel) * inverseOpacity, i, j);

					canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
				}
			}

			processorListener.exportCanvasTrueColor();
		}
		else {
			ConsoleLogger.getInstance().error("Image sizes mismatch, cannot mix images");
		}
	}

	@Override
	public void execute() {
		if (maskImage != null) {
			processLayerMaskMixing();
		}
		else if (opacity == Integer.MIN_VALUE) {
			processSimpleMixing();
		}
		else {
			processOpacityMixing();
		}
	}
}
