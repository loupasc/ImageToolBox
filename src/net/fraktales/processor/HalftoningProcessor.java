package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.DitherQuantization;
import net.fraktales.param.OrderedDitherArg;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.ProgressLog;

public class HalftoningProcessor extends AbstractProcessor {

	private final OrderedDitherProcessor pixelProcessor;
	private final int colorScale;

	public HalftoningProcessor(ProcessorListener processorListener, OrderedDitherArg ditherMethod, DitherQuantization quantization) {
		super(processorListener);
		pixelProcessor = createOrderedDitherProcessor(ditherMethod, quantization.colorCount);
		colorScale = 255 / (quantization.colorCount - 1);
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		ProgressLog halftoningProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			halftoningProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int r = colorScale * pixelProcessor.applyOrderedDithering(BufferedImageUtils.getRed(pixelColor), i, j);
				int g = colorScale * pixelProcessor.applyOrderedDithering(BufferedImageUtils.getGreen(pixelColor), i, j);
				int b = colorScale * pixelProcessor.applyOrderedDithering(BufferedImageUtils.getBlue(pixelColor), i, j);

				canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
			}
		}

		if (colorScale == 255) {
			processorListener.exportCanvasIndexedColor();
		}
		else {
			processorListener.exportCanvasTrueColor();
		}
	}

	private static interface OrderedDitherProcessor {
		public int applyOrderedDithering(int value, int x, int y);
	}

	private static OrderedDitherProcessor createOrderedDitherProcessor(OrderedDitherArg ditherMethod, final int colorCount) {
		switch (ditherMethod) {
			case CHECKS:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyChecksDithering(value, colorCount, x, y);
					}
				};
			case DISPERSED:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyDispersedDotDithering(value, colorCount, x, y);
					}
				};
			case ARCADE:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyArcadeDithering(value, colorCount, y);
					}
				};
			case DEFAULT:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyTilesDithering(value, colorCount, x, y);
					}
				};
			case LINES:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyLinesDithering(value, colorCount, x, y);
					}
				};
			case RANDOM:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyRandomDithering(value, colorCount, x, y);
					}
				};
			case VOID_AND_CLUSTER:
				return new OrderedDitherProcessor() {
					@Override
					public int applyOrderedDithering(int value, int x, int y) {
						return DitherOperator.applyVoidAndClusterDithering(value, colorCount, x, y);
					}
				};
		}

		throw new IllegalArgumentException("Invalid ordered dither method: " + ditherMethod);
	}
}
