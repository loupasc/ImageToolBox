package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.KernelShape;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.GaussianUnsharpMask;
import net.fraktales.tools.ProgressLog;
import net.fraktales.tools.SincFilter;

public class UpscalerProcessor extends AbstractProcessor {
	// Coefficents are scaled in order to get sum(matrix) = 2^24
	// and Coefficient * 255 < Integer.MAX_VALUE (safe multiply)
	private static final int KERNEL_COEF_SCALE = 4184117;
	private static final int[] ZEROS_LINE = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private final int[][] kernelMatrix;
	private final Canvas input;

	public UpscalerProcessor(ProcessorListener processorListener, Canvas input, boolean addSharpness) {
		super(processorListener);
		this.input = input;
		kernelMatrix = createKernelMatrix(addSharpness);
	}

	private static int[][] createKernelMatrix(boolean addSharpness) {
		double[][] lanczos4Matrix = SincFilter.createLanczosKernel(KernelShape.SQUARE, 4, 8);
		if (addSharpness) {
			double[][] lanczos = new double[15][15];
			lanczos[7][7] = lanczos4Matrix[0][0];
			for (int i = 1; i < 8; i++) {
				lanczos[7 + i][7] = lanczos4Matrix[i][0];
				lanczos[7 - i][7] = lanczos4Matrix[i][0];
				lanczos[7][7 + i] = lanczos4Matrix[i][0];
				lanczos[7][7 - i] = lanczos4Matrix[i][0];
			}
			for (int j = 1; j < 8; j++) {
				for (int i = 1; i < 8; i++) {
					lanczos[7 + i][7 + j] = lanczos4Matrix[i][j];
					lanczos[7 - i][7 + j] = lanczos4Matrix[i][j];
					lanczos[7 + i][7 - j] = lanczos4Matrix[i][j];
					lanczos[7 - i][7 - j] = lanczos4Matrix[i][j];
				}
			}
			lanczos = GaussianUnsharpMask.convolveKernelMatrix(lanczos);
			int matrixLength = lanczos.length;
			int[][] matrix = new int[matrixLength][matrixLength];
			for (int j = 0; j < matrixLength; j++) {
				for (int i = 0; i < matrixLength; i++) {
					matrix[i][j] = (int) Math.round(KERNEL_COEF_SCALE * lanczos[i][j]);
				}
			}
			matrix[11][11] = matrix[11][11] - 2;
			return matrix;
		}

		int[][] matrix = new int[15][15];
		matrix[7][7] = KERNEL_COEF_SCALE + 19; // round(scale * lanczos4Matrix[0][0]);
		for (int i = 1; i < 8; i++) {
			int coef = (int) Math.round(KERNEL_COEF_SCALE * lanczos4Matrix[i][0]);
			matrix[7 + i][7] = coef;
			matrix[7 - i][7] = coef;
			matrix[7][7 + i] = coef;
			matrix[7][7 - i] = coef;
		}
		for (int j = 1; j < 8; j++) {
			for (int i = 1; i < 8; i++) {
				int coef = (int) Math.round(KERNEL_COEF_SCALE * lanczos4Matrix[i][j]);
				matrix[7 + i][7 + j] = coef;
				matrix[7 - i][7 + j] = coef;
				matrix[7 + i][7 - j] = coef;
				matrix[7 - i][7 - j] = coef;
			}
		}

		for (int i = 0; i < 3; i++) {
			matrix[5 - 2 * i] = ZEROS_LINE;
			matrix[9 + 2 * i] = ZEROS_LINE;
		}

		return matrix;
	}

	private static int clamp(long value) {
		// Clipping
		if (value < 0) {
			return 0;
		}
		if (value > 4269801471L) {
			return 255;
		}
		// rounding
		return (int) ((value + 8388608) >> 24);
	}

	private static int convolveInput(Canvas input, int x, int y, int[][] kernelMatrix) {
		// Assuming dealing with square matrix
		int matrixLength = kernelMatrix.length;
		int shift = matrixLength / 2;
		long r = 0;
		long g = 0;
		long b = 0;
		for (int j = 0; j < matrixLength; j++) {
			if (kernelMatrix[j] != ZEROS_LINE) {
				int posY = (y + j - shift) / 2;
				for (int i = 0; i < matrixLength; i++) {
					int coef = kernelMatrix[j][i];
					if (coef != 0) {
						int posX = (x + i - shift) / 2;
						int pixelColor = input.safeGetRGB(posX, posY);
						r = r + coef * BufferedImageUtils.getRed(pixelColor);
						g = g + coef * BufferedImageUtils.getGreen(pixelColor);
						b = b + coef * BufferedImageUtils.getBlue(pixelColor);
					}
				}
			}
		}

		return BufferedImageUtils.packRgbBytes(clamp(r), clamp(g), clamp(b));
	}

	@Override
	public void execute() {
		ProgressLog.loadingImage("input image", input.width, input.height);
		Canvas canvas = processorListener.getCanvas();

		ProgressLog upscalerProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			upscalerProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = convolveInput(input, i, j, kernelMatrix);
				canvas.setRGB(i, j, pixelColor);
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
