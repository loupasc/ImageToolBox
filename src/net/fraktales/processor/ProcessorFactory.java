package net.fraktales.processor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import net.fraktales.image.Canvas;
import net.fraktales.output.ImageFileWriter;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.DitherQuantization;
import net.fraktales.param.MaskParameter;
import net.fraktales.parser.AbstractParser;
import net.fraktales.parser.BlockifierParser;
import net.fraktales.parser.BlurParser;
import net.fraktales.parser.ColorizerParser;
import net.fraktales.parser.ContrastMatcherParser;
import net.fraktales.parser.DitheringParser;
import net.fraktales.parser.Filter;
import net.fraktales.parser.GradientMapperParser;
import net.fraktales.parser.HalftoningParser;
import net.fraktales.parser.ImageMixingParser;
import net.fraktales.parser.ImageProcessingParser;
import net.fraktales.parser.SharpenParser;
import net.fraktales.parser.SigmoidalContrastParser;
import net.fraktales.parser.SincParser;
import net.fraktales.parser.UpscalerParser;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ConsoleLogger;

public final class ProcessorFactory {

	private ProcessorFactory() {
	}

	private static ImageProcessor createImageProcessor(ProcessorListener processorListener, ImageProcessingParser parser) {
		return new ImageProcessor(processorListener,
				parser.getChannelMixerParameters(),
				parser.getContrastLevelParameters(),
				parser.getParabolicCurveParameters(),
				parser.getSigmoidalContrast());
	}

	private static BlockifierProcessor createBlockifierProcessor(ProcessorListener processorListener, BlockifierParser parser) {
		return new BlockifierProcessor(processorListener, parser.isTinyPatternEnabled(), parser.isSofterRenderingEnabled());
	}

	private static BlurProcessor createBlurProcessor(ProcessorListener processorListener, BlurParser parser) {
		return new BlurProcessor(processorListener, parser.getBlurSettings());
	}

	private static ColorizerProcessor createColorizerProcessor(ProcessorListener processorListener, ColorizerParser parser) throws IOException {
		Canvas coloredImage = BufferedImageUtils.createCanvasFromFile(new File(parser.getColoredImageFileName()));
		return new ColorizerProcessor(processorListener, coloredImage);
	}

	private static ContrastMatcherProcessor createContrastMatcherProcessor(ProcessorListener processorListener, ContrastMatcherParser parser) throws IOException {
		Canvas referenceImage = BufferedImageUtils.createCanvasFromFile(new File(parser.getReferenceFileName()));
		return new ContrastMatcherProcessor(processorListener, referenceImage);
	}

	private static GradientMapperProcessor createGradientMapperProcessor(ProcessorListener processorListener, GradientMapperParser parser) {
		return new GradientMapperProcessor(processorListener, parser.getColors(), parser.isSofterRenderingEnabled());
	}

	private static HalftoningProcessor createHalftoningProcessor(ProcessorListener processorListener, HalftoningParser parser) {
		return new HalftoningProcessor(processorListener, parser.getOrderedDitherArg(), parser.getQuantization());
	}

	private static AbstractProcessor createDitheringProcessor(ProcessorListener processorListener, DitheringParser parser) {
		if (parser.getQuantization() == DitherQuantization.BINARY) {
			return new DitheringProcessor(processorListener);
		}
		return new WebSafeDitheringProcessor(processorListener);
	}

	private static ImageMixingProcessor createImageMixingProcessor(ProcessorListener processorListener, ImageMixingParser parser) throws IOException {
		Canvas secondImage = BufferedImageUtils.createCanvasFromFile(new File(parser.getSecondFileName()));
		MaskParameter maskParameter = parser.getMaskParameter();

		if (maskParameter.isOpacity) {
			return new ImageMixingProcessor(processorListener, secondImage, maskParameter.opacity);
		}
		if (maskParameter.filename == null) {
			return new ImageMixingProcessor(processorListener, secondImage);
		}

		Canvas maskImage = BufferedImageUtils.createCanvasFromFile(new File(maskParameter.filename));
		return new ImageMixingProcessor(processorListener, secondImage, maskImage);
	}

	private static SharpenProcessor createSharpenProcessor(ProcessorListener processorListener, SharpenParser parser) {
		return new SharpenProcessor(processorListener, parser.getStrength(), parser.isLumaOnlyEnabled());
	}

	private static SincProcessor createSincProcessor(ProcessorListener processorListener, SincParser parser) {
		return new SincProcessor(processorListener,
				parser.getKernelShape(),
				parser.getCutoff(),
				parser.getRadius(),
				parser.getContrast());
	}

	private static SigmoidalContrastProcessor createSigmoidalContrastProcessor(ProcessorListener processorListener, SigmoidalContrastParser parser) {
		return new SigmoidalContrastProcessor(processorListener, parser.getSigmoidalContrast());
	}

	public static AbstractProcessor createFromParser(AbstractParser parser) {

		try {
			Canvas canvas = BufferedImageUtils.createCanvasFromFile(new File(parser.getInputFileName()));
			ImageFileWriter fileWriter = (parser.getFilter() == Filter.UPSCALER2X
					? new ImageFileWriter(new Canvas(new BufferedImage(2 * canvas.width, 2 * canvas.height, BufferedImage.TYPE_INT_RGB)), parser.getOutputFileName())
					: new ImageFileWriter(canvas, parser.getOutputFileName()));

			switch (parser.getFilter()) {
			case BW_LEVELS:
				ImageProcessingParser imageProcessingParser = (ImageProcessingParser) parser;
				return createImageProcessor(fileWriter, imageProcessingParser);
			case BLOCKIFIER:
				BlockifierParser blockifierParser = (BlockifierParser) parser;
				return createBlockifierProcessor(fileWriter, blockifierParser);
			case BLUR:
				BlurParser blurParser = (BlurParser) parser;
				return createBlurProcessor(fileWriter, blurParser);
			case COLORIZER:
				ColorizerParser colorizerParser = (ColorizerParser) parser;
				return createColorizerProcessor(fileWriter, colorizerParser);
			case GRADIENT_MAPPER:
				GradientMapperParser gradientMapperParser = (GradientMapperParser) parser;
				return createGradientMapperProcessor(fileWriter, gradientMapperParser);
			case LEVELS_AUTO:
				return new LevelsAutoProcessor(fileWriter);
			case HALFTONING:
				HalftoningParser halftoningParser = (HalftoningParser) parser;
				return createHalftoningProcessor(fileWriter, halftoningParser);
			case DITHER:
				DitheringParser ditheringParser = (DitheringParser) parser;
				return createDitheringProcessor(fileWriter, ditheringParser);
			case MATCHER:
				ContrastMatcherParser contrastMatcherParser = (ContrastMatcherParser) parser;
				return createContrastMatcherProcessor(fileWriter, contrastMatcherParser);
			case MIXING:
				ImageMixingParser imageMixingParser = (ImageMixingParser) parser;
				return createImageMixingProcessor(fileWriter, imageMixingParser);
			case SHARPEN:
				SharpenParser sharpenParser = (SharpenParser) parser;
				return createSharpenProcessor(fileWriter, sharpenParser);
			case SINC:
				SincParser sincParser = (SincParser) parser;
				return createSincProcessor(fileWriter, sincParser);
			case SIGMOIDAL_CONTRAST:
				SigmoidalContrastParser sigmoidalContrastParser = (SigmoidalContrastParser) parser;
				return createSigmoidalContrastProcessor(fileWriter, sigmoidalContrastParser);
			case UPSCALER2X:
				UpscalerParser upscalerParser = (UpscalerParser) parser;
				return new UpscalerProcessor(fileWriter, canvas, upscalerParser.isAddSharpness());
			case NULL:
				break;
			}
		}
		catch (IOException e) {
			ConsoleLogger.getInstance().error(e.getMessage());
		}

		return new AbstractProcessor(null) {
			@Override
			public void execute() {}
		};
	}
}
