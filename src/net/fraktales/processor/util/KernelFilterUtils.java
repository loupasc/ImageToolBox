package net.fraktales.processor.util;

import net.fraktales.processor.util.GaussianHvsModel.Preset;

public final class KernelFilterUtils {

	//
	// Gauss kernel 3x3 to 9x9 customized kernel for the rest of the area
	//
	private static final KernelFilter[] KERNEL_ARRAY = createKernelFilterArray();

	/**
	 * State-less class => No need to create instances.
	 */
	private KernelFilterUtils() {
	}

	private static KernelFilter[] createKernelFilterArray() {
		int[][] kernelMatrix = GaussianHvsModel.createMatrix(Preset.M9x9);
		KernelFilter[] kernelFilterArray = new KernelFilter[4];

		kernelFilterArray[0] = new KernelFilter(3);
		kernelFilterArray[1] = new KernelFilter(5);
		kernelFilterArray[2] = new KernelFilter(7);
		kernelFilterArray[3] = new KernelFilter(kernelMatrix);

		return kernelFilterArray;
	}

	public static KernelFilter getKernelFilterToToggle(int x, int y, int width, int height) {
		if ((x == 1) || (x == width - 2) || (y == 1) || (y == height - 2)) {
			return KERNEL_ARRAY[0];
		}
		if ((x == 2) || (x == width - 3) || (y == 2) || (y == height - 3)) {
			return KERNEL_ARRAY[1];
		}
		if ((x == 3) || (x == width - 4) || (y == 3) || (y == height - 4)) {
			return KERNEL_ARRAY[2];
		}
		return KERNEL_ARRAY[3];
	}

	public static KernelFilter getKernelFilterToSwap(int x, int y, int width, int height) {
		if ((x == 1) || (x == width - 3) || (y == 1) || (y == height - 2)) {
			return KERNEL_ARRAY[0];
		}
		if ((x == 2) || (x == width - 4) || (y == 2) || (y == height - 3)) {
			return KERNEL_ARRAY[1];
		}
		if ((x == 3) || (x == width - 5) || (y == 3) || (y == height - 4)) {
			return KERNEL_ARRAY[2];
		}
		return KERNEL_ARRAY[3];
	}
}
