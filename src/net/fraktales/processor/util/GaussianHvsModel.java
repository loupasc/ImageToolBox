package net.fraktales.processor.util;

/**
 * This class generates convolution kernel in order to be used for blurring image.
 * The convolution kernel is represented by a linearized Integer matrix.
 * The Gaussian blurring is used to look like human vision.
 * Kernel can be used as a model of Human Visual System.
 * 
 * @author Pascal Ollive
 * @since Apr 28, 2020
 */
public final class GaussianHvsModel {
	/**
	 * Matrix generator configuration preset.
	 */
	public static enum Preset {
		M3x3(1, 304, 912),
		M5x5(2, 551.5, 1656),
		M7x7(3, 1039.5, 3120),
		M9x9(4, 2354, 7064),
		M11x11(5, 6047.84, 18144),
		M13x13(6, 16843.92, 50532),
		M15x15(7, 49599.9, 148800),
		M17x17(8, 152138.33, 456420),
		M19x19(9, 481441.93, 1444328),
		M21x21(10, 1561492.3, 4684484);

		public final int radius;
		private final double scale;
		private final int patchCenter;

		private Preset(int radius, double scale, int patchCenter) {
			this.radius = radius;
			this.scale = scale;
			this.patchCenter = patchCenter;
		}
	}

	/**
	 * State-less class => No need to create instances.
	 */
	private GaussianHvsModel() {
	}

	// Human Visual System Model:
	// G(x,y) = 2 * exp(-4.88 * x^2+y^2 / 1.5) + exp(-4.88 * x^2+y^2 / 8)
	private static double gaussian(double x, double y) {
		double sqr = x * x + y * y;
		return 2 * Math.exp(-4.88 * sqr / 1.5) + Math.exp(-4.88 * sqr / 8);
	}

	//
	// Internal method for creating preset matrix
	//
	private static int[][] createMatrix(double scale, int radius, int patchCenter) {
		int[][] matrix = createMatrix(scale, radius);
		//
		// Patch center value
		matrix[radius][radius] = patchCenter;

		return matrix;
	}

	/**
	 * Matrix generator expert settings.
	 * 
	 * @param scale matrix coefficient scale
	 * @param radius filter radius
	 * @return convolution kernel
	 */
	public static int[][] createMatrix(double scale, int radius) {
		int dimension = 2 * radius + 1;
		int[][] matrix = new int[dimension][dimension];

		for (int j = 0; j < dimension; j++) {
			for (int i = 0; i < dimension; i++) {
				double gxy = gaussian(2. * (i - radius) / radius, 2. * (j - radius) / radius);
				matrix[i][j] = (int) Math.round(scale * gxy);
			}
		}

		return matrix;
	}

	/**
	 * Matrix generator based on preset. Recommended.
	 * 
	 * @param preset generator preset configuration
	 * @return convolution kernel
	 */
	public static int[][] createMatrix(Preset preset) {
		return createMatrix(preset.scale, preset.radius, preset.patchCenter);
	}

	// -- TEST --
//	private static void displayCoef(Preset p) {
//		int[][] m = createMatrix(p);
//		int dimension = m.length;
//		int validSum = 1 << (2 * p.radius + 8);
//		int sum = 0;
//		for (int j = 0; j < dimension; j++) {
//			System.out.println();
//			for (int i = 0; i < dimension; i++) {
//				System.out.print(m[i][j] + " ");
//				sum = sum + m[i][j];
//			}
//		}
//		System.out.println();
//		System.out.println(sum);
//		System.out.println(validSum == sum);
//	}
//
//	public static void main(String[] args) {
//		displayCoef(Preset.M9x9);
//	}
}
