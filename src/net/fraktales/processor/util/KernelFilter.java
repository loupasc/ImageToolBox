package net.fraktales.processor.util;

import java.util.Arrays;

import net.fraktales.image.Canvas;

public class KernelFilter {
    private final int radius;
    public final int dimension;
	public final int centerPosition;
	private final int[] matrix;
	private final int norm;
	/** Using for rounding with floating point. */
    private final int sumDiv2;

    private KernelFilter(int dimension, int[] matrix) {
        radius = dimension / 2;
		this.dimension = dimension;
		this.matrix = matrix;

		int sum = 0;
		for (int i = 0; i < matrix.length; i++) {
			sum = sum + this.matrix[i];
		}

		sumDiv2 = sum / 2;

		int powerOf2 = 1;
		int exponent = 0;
		while (powerOf2 < sum) {
			powerOf2 = 2 * powerOf2;
			exponent = exponent + 1;
		}
		norm = exponent;

		centerPosition = matrix.length / 2;
	}

	public KernelFilter(int dimension) {
		this(dimension, createNxNKernelMatrix(dimension));
	}

	public KernelFilter(int[][] matrix) {
		this(9, clone9x9CustomMatrix(matrix));
	}

	private static int[] createNxNKernelMatrix(int dimension) {
		int[] matrix = new int[dimension * dimension];

		matrix[0] = 1;
		for (int j = 0; j < (dimension - 1); j++) {
			for (int i = j; i >= 0; i--) {
				matrix[i + 1] = matrix[i + 1] + matrix[i];
			}
		}
		for (int j = 0; j < dimension; j++) {
			int coef = matrix[j];
			int offset = j * dimension;
			for (int i = 0; i < dimension; i++) {
				matrix[offset + i] = coef * matrix[i];
			}
		}

		return matrix;
	}

	private static int[] clone9x9CustomMatrix(int[][] matrix) {
		if (matrix.length != 9) {
            // Assuming dealing with square matrix
			throw new IllegalArgumentException("Only 9x9 dimension matrix allowed to define custom kernel");
        }
        int[] kernelMatrix = new int[81];
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                kernelMatrix[9 * j + i] = matrix[i][j];
            }
        }
		return kernelMatrix;
    }

    public int convolvePixel(int x, int y, Canvas image) {
        int offset = 0;
        long pixel = 0;

        for (int j = 0; j < dimension; j++) {
            int posY = y - radius + j;
            for (int i = 0; i < dimension; i++) {
                int posX = x - radius + i;
                // Assuming dealing with monochrome image
                pixel = pixel + matrix[offset] * image.getRGB(posX, posY);
                offset = offset + 1;
            }
        }

        return (int) ((pixel + sumDiv2) >> norm);
    }

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder("KernelFilter [matrix=");
		sb.append(Arrays.toString(matrix));
		sb.append(", size=");
		sb.append(matrix.length);
		sb.append(", norm=");
		sb.append(norm);
		sb.append(", centerPosition=");
		sb.append(centerPosition);
		sb.append(", dimension=");
		sb.append(dimension);
		sb.append(", sumDiv2=");
		sb.append(sumDiv2);
		sb.append(']');
		return sb.toString();
	}
}