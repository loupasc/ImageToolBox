package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.KernelShape;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ProgressLog;
import net.fraktales.tools.SincFilter;

public class SincProcessor extends AbstractProcessor {

	private final double[][] kernelMatrix;
	private final double kernelNorm;
	private final int contrastFactor;

	public SincProcessor(ProcessorListener processorListener, KernelShape kernelShape, int cutoffFactor, int radius, int contrastFactor) {
		super(processorListener);
		this.contrastFactor = contrastFactor;
		kernelMatrix = SincFilter.createLanczosKernel(kernelShape, 4 * cutoffFactor, (radius * cutoffFactor) / 2);
		kernelNorm = SincFilter.getCoefficientSum(kernelMatrix);
	}

	private static int to8bit(double x) {
		if (x < 0) {
			return 0;
		}
		if (x > 255) {
			return 255;
		}
		return (int) Math.round(x);
	}

	private static int packRgbBytes(double red, double green, double blue) {
		return BufferedImageUtils.packRgbBytes(to8bit(red),
				to8bit(green),
				to8bit(blue));
	}

	private double applyContrast(double x) {
		return (256 + 10 * (contrastFactor - 5)) * x / 256 - (5 * (contrastFactor - 5));
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		Canvas input = canvas.clone();
		ProgressLog.loadingImage("input image", input.width, input.height);
		// Assuming dealing with square matrix
		int r = kernelMatrix.length;

		ProgressLog sincProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			sincProgess.display(j);
			boolean isCenterY = ((j >= r) && (j < canvas.height - r));
			int i = 0;
			while (i < r) {
				double[] rgb = SincFilter.convolve(input, i, j, false, kernelMatrix, r);
				int pixelColor = packRgbBytes(applyContrast(rgb[0] / kernelNorm),
						applyContrast(rgb[1] / kernelNorm),
						applyContrast(rgb[2] / kernelNorm));
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
			while (i < canvas.width - r) {
				double[] rgb = SincFilter.convolve(input, i, j, isCenterY, kernelMatrix, r);
				int pixelColor = packRgbBytes(applyContrast(rgb[0] / kernelNorm),
						applyContrast(rgb[1] / kernelNorm),
						applyContrast(rgb[2] / kernelNorm));
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
			while (i < canvas.width) {
				double[] rgb = SincFilter.convolve(input, i, j, false, kernelMatrix, r);
				int pixelColor = packRgbBytes(applyContrast(rgb[0] / kernelNorm),
						applyContrast(rgb[1] / kernelNorm),
						applyContrast(rgb[2] / kernelNorm));
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
