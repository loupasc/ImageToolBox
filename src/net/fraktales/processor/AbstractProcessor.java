package net.fraktales.processor;

import net.fraktales.output.ProcessorListener;

public abstract class AbstractProcessor {

	protected final ProcessorListener processorListener;

	public AbstractProcessor(ProcessorListener processorListener) {
		this.processorListener = processorListener;
	}

	public abstract void execute();
}
