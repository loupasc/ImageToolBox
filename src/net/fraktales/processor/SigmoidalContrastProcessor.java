package net.fraktales.processor;

import java.util.Arrays;

import net.fraktales.image.Canvas;
import net.fraktales.image.Canvas.Channel;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.ProgressLog;
import net.fraktales.tools.SigmoidFunction;

public class SigmoidalContrastProcessor extends AbstractProcessor {

	private static final Channel[] COLOR_CHANNEL_VALUES = Channel.values();
	private static final DitherOperator DITHER_OP = new DitherOperator(25500);// Percentage of 8-bit value
	private static final short UNDEFINED_VALUE = Short.MIN_VALUE;
	private final short[] cache = new short[256];
	private final SigmoidFunction sigmoid;

	public SigmoidalContrastProcessor(ProcessorListener processorListener, double sigmoidalContrast) {
		super(processorListener);
		sigmoid = new SigmoidFunction(sigmoidalContrast);
		Arrays.fill(cache, UNDEFINED_VALUE);
	}

	private int applyContrast(int rgb, int x, int y) {
		int rgbColor = 0;
		for (Channel channel : COLOR_CHANNEL_VALUES) {
			int intensity = (rgb >> channel.bandShift) & 0xFF;
			if (cache[intensity] == UNDEFINED_VALUE) {
				cache[intensity] = (short) sigmoid.get(intensity * 100);
			}
			rgbColor = rgbColor | (DITHER_OP.apply8bitsDithering(cache[intensity], x, y) << channel.bandShift);
		}
		return rgbColor;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		ProgressLog contrastProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			contrastProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				canvas.setRGB(i, j, applyContrast(pixelColor, i, j));
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
