package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.ConsoleLogger;
import net.fraktales.tools.ProgressLog;

public class ColorizerProcessor extends AbstractProcessor {

	private final Canvas coloredImage;

	public ColorizerProcessor(ProcessorListener processorListener, Canvas coloredImage) {
		super(processorListener);
		this.coloredImage = coloredImage;
	}

	private static int clamp(double x) {
		if (x < 0) {
			return 0;
		}
		if (x > 255) {
			return 255;
		}
		return (int) Math.round(x);
	}

	private static void processBufferedImages(Canvas canvas, Canvas coloredImage) {
		int width = canvas.width;
		int height = canvas.height;
		ProgressLog.loadingImage("monochrome image", width, height);
		ProgressLog.loadingImage("colored image", width, height);

		ProgressLog colorizingProgess = new ProgressLog(height);

		for (int j = 0; j < height; j++) {
			colorizingProgess.display(j);
			for (int i = 0; i < width; i++) {
				int pixelColor = coloredImage.getRGB(i, j);
				int luma = BufferedImageUtils.getRed(canvas.getRGB(i, j));
				int r = BufferedImageUtils.getRed(pixelColor);
				int g = BufferedImageUtils.getGreen(pixelColor);
				int b = BufferedImageUtils.getBlue(pixelColor);

				double u = ColorTransformation.rgbToU(r, g, b);
				double v = ColorTransformation.rgbToV(r, g, b);

				r = clamp(ColorTransformation.yuvToR(luma, v));
				g = clamp(ColorTransformation.yuvToG(luma, u, v));
				b = clamp(ColorTransformation.yuvToB(luma, u));

				canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(r, g, b));
			}
		}
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		if ((canvas.width == coloredImage.width) && (canvas.height == coloredImage.height)) {
			processBufferedImages(canvas, coloredImage);
			processorListener.exportCanvasTrueColor();
		}
		else {
			ConsoleLogger.getInstance().error("Image sizes mismatch, cannot process images");
		}
	}
}
