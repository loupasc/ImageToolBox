package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.ChannelMixer;
import net.fraktales.param.ContrastLevels;
import net.fraktales.param.ParabolicCurve;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.ProgressLog;
import net.fraktales.tools.SigmoidFunction;

public class ImageProcessor extends AbstractProcessor {

	private final ChannelMixer bw;
	private final ParabolicCurve pc;
	private final SigmoidFunction sigmoid;
	private final int levelDenominator;
	private final int contrastLevelMinNorm;

	public ImageProcessor(ProcessorListener processorListener, ChannelMixer bw, ContrastLevels levels, ParabolicCurve pc, double sigmoidalContrast) {
		super(processorListener);
		this.bw = bw;
		this.pc = pc;
		this.sigmoid = sigmoidalContrast > 0 ? new SigmoidFunction(sigmoidalContrast) : null;
		levelDenominator = levels.max - levels.min;
		contrastLevelMinNorm = levels.min * 100;
	}

	private int applyProcessing(int intensity) {
		// Apply contrast
		intensity = ((intensity - contrastLevelMinNorm) * 255) / levelDenominator;

		// Clamp before
		// Because parabolic curve is not linear and decreases after maximum
		if (intensity < 0) intensity = 0;
		if (intensity > 25500) intensity = 25500;

		if (pc.count != 0) {
			intensity = ColorTransformation.parabola(pc.x, pc.y, intensity);
		}

		if (sigmoid != null) {
			intensity = sigmoid.get(intensity);
		}

		// Normalization
		// (value + 50) / 100 <=> Math.round(value / 100)
		intensity = (intensity + 50) / 100;

		// post processing clamp 
		if (intensity > 255) {
			return 255; 
		}
		return intensity;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		ProgressLog processingProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			processingProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int gray = applyProcessing(bw.r * BufferedImageUtils.getRed(pixelColor)
						+ bw.g * BufferedImageUtils.getGreen(pixelColor)
						+ bw.b * BufferedImageUtils.getBlue(pixelColor));

				canvas.setRGB(i, j, gray);
			}
		}

		processorListener.exportCanvasMonochrom();
	}
}
