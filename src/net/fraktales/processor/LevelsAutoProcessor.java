package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.image.Canvas.Channel;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.HistogramUtils;
import net.fraktales.tools.ProgressLog;

public class LevelsAutoProcessor extends AbstractProcessor {

	private static final Channel[] COLOR_CHANNEL_VALUES = Channel.values();
	private static final DitherOperator DITHER_OP = new DitherOperator(25500);// Percentage of 8-bit value
	private static final int CLIPPING_RATIO = 10000;

	public LevelsAutoProcessor(ProcessorListener processorListener) {
		super(processorListener);
	}

	private static int min(int a, int b, int c) {
		int result = (a < b ? a : b);
		return (result < c ? result : c);
	}

	private static int max(int a, int b, int c) {
		int result = (a > b ? a : b);
		return (result > c ? result : c);
	}

	private static int applyContrast(int min, int max, int intensity) {
		if (intensity < min) {
			return 0;
		}
		if (intensity > max) {
			return 25500;
		}
		return ((intensity - min) * 25500) / (max - min);
	}

	private static int getMinFromHisto(int[] histogram, int waste) {
		int i = 0;
		int pixelCount = histogram[0];
		while (pixelCount < waste) {
			i = i + 1;
			pixelCount = pixelCount + histogram[i];
		}
		return i;
	}

	private static int getMaxFromHisto(int[] histogram, int waste) {
		int i = histogram.length;
		int pixelCount = 0;
		while (pixelCount < waste) {
			i = i - 1;
			pixelCount = pixelCount + histogram[i];
		}
		return i;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);

		int waste = (canvas.width * canvas.height) / CLIPPING_RATIO;
		int[] min = new int[COLOR_CHANNEL_VALUES.length];
		int[] max = new int[COLOR_CHANNEL_VALUES.length];
		byte[] bitmapChannel = new byte[canvas.width * canvas.height];

		for (Channel channel : COLOR_CHANNEL_VALUES) {
			canvas.surfaceCopy(channel, 0, bitmapChannel);
			int[] histo = HistogramUtils.createHistogram(bitmapChannel);
			min[channel.ordinal()] = getMinFromHisto(histo, waste);
			max[channel.ordinal()] = getMaxFromHisto(histo, waste);
		}

		bitmapChannel = null;

		int minIntensity = min(min[Channel.RED.ordinal()], min[Channel.GREEN.ordinal()], min[Channel.BLUE.ordinal()]);
		int maxIntensity = max(max[Channel.RED.ordinal()], max[Channel.GREEN.ordinal()], max[Channel.BLUE.ordinal()]);

		min = null;
		max = null;

		ProgressLog levelsAutoProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			levelsAutoProgess.display(j);
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int r = applyContrast(minIntensity, maxIntensity, BufferedImageUtils.getRed(pixelColor));
				int g = applyContrast(minIntensity, maxIntensity, BufferedImageUtils.getGreen(pixelColor));
				int b = applyContrast(minIntensity, maxIntensity, BufferedImageUtils.getBlue(pixelColor));

				canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(DITHER_OP.apply8bitsDithering(r, i, j),
						DITHER_OP.apply8bitsDithering(g, i, j),
						DITHER_OP.apply8bitsDithering(b, i, j)));
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
