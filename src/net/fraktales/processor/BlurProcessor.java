package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.processor.util.GaussianHvsModel;
import net.fraktales.processor.util.GaussianHvsModel.Preset;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.ProgressLog;

public class BlurProcessor extends AbstractProcessor {

	private final Preset blurSettings;

	public BlurProcessor(ProcessorListener processorListener, Preset blurSettings) {
		super(processorListener);
		this.blurSettings = blurSettings;
	}

	private static int getBlurredPixelValue(Canvas image, int x, int y, boolean isCentralArea, KernelInfo k) {
		// Assuming dealing with square matrix
		int dimension = k.matrix.length;
		int ditherNoise = DitherOperator.blueNoise(k.noiseAmount, x, y);
		long r = 0;
		long g = 0;
		long b = 0;
		for (int j = 0; j < dimension; j++) {
			int posY = y - k.radius + j;
			for (int i = 0; i < dimension; i++) {
				int coef = k.matrix[i][j];
				int posX = x - k.radius + i;
				int pixelColor = (isCentralArea ? image.getRGB(posX, posY) : image.safeGetRGB(posX, posY));
				r = r + BufferedImageUtils.getRed(pixelColor) * coef;
				g = g + BufferedImageUtils.getGreen(pixelColor) * coef;
				b = b + BufferedImageUtils.getBlue(pixelColor) * coef;
			}
		}

		r = (r + ditherNoise) >> k.norm;
		g = (g + ditherNoise) >> k.norm;
		b = (b + ditherNoise) >> k.norm;

		return BufferedImageUtils.packRgbBytes((int) r, (int) g, (int) b);
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		Canvas input = canvas.clone();
		ProgressLog.loadingImage("input image", input.width, input.height);

		KernelInfo kernelInfo = new KernelInfo(blurSettings);
		int r = blurSettings.radius;

		ProgressLog blurProgess = new ProgressLog(canvas.height);
		for (int j = 0; j < canvas.height; j++) {
			blurProgess.display(j);
			boolean isCenterY = ((j >= r) && (j < canvas.height - r));
			int i = 0;

			while (i < r) {
				int pixelColor = getBlurredPixelValue(input, i, j, false, kernelInfo);
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
			while (i < canvas.width - r) {
				int pixelColor = getBlurredPixelValue(input, i, j, isCenterY, kernelInfo);
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
			while (i < canvas.width) {
				int pixelColor = getBlurredPixelValue(input, i, j, false, kernelInfo);
				canvas.setRGB(i, j, pixelColor);
				i = i + 1;
			}
		}

		processorListener.exportCanvasTrueColor();
	}

	private static final class KernelInfo {
		private final int[][] matrix;
		private final int radius;
		private final int norm;
		private final int noiseAmount;

		public KernelInfo(Preset blurSettings) {
			this.matrix = GaussianHvsModel.createMatrix(blurSettings);
			this.radius = blurSettings.radius;
			norm = 2 * radius + 8;
			noiseAmount = 1 << norm;
		}
	}
}
