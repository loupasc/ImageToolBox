package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.DitherOperator;
import net.fraktales.tools.HistogramUtils;
import net.fraktales.tools.ProgressLog;

public class ContrastMatcherProcessor extends AbstractProcessor {

	private static final long DEEP_WHITE_INTENSITY = ColorTransformation.rgbToDeepGray(255, 255, 255);

	private final Canvas referenceImage;

	public ContrastMatcherProcessor(ProcessorListener processorListener, Canvas referenceImage) {
		super(processorListener);
		this.referenceImage = referenceImage;
	}

	private static byte[] createTileFromCanvas(Canvas canvas) {
		int tileHeight = canvas.height / 2;

		return new byte[canvas.width * tileHeight];
	}

	private static int[] createLookUpTable(int[] sourceCH, int[] targetCH) {
		int cumulativeHistogramLength = sourceCH.length;
		int[] lookupTable = new int[cumulativeHistogramLength];
		int intensitySup = 0;

		for (int i = 0; i < cumulativeHistogramLength; i++) {
			int pixelCount = sourceCH[i];
			while (targetCH[intensitySup] < pixelCount) {
				intensitySup = intensitySup + 1;
			}

			long intensity = 0;
			if (intensitySup > 0) {
				int intensityInf = intensitySup - 1;
				int intervalLength = targetCH[intensitySup] - targetCH[intensityInf];
				intensity = ((((pixelCount - targetCH[intensityInf]) * intensitySup + (targetCH[intensitySup] - pixelCount) * intensityInf)) * DEEP_WHITE_INTENSITY)
										/ (255 * intervalLength);
			}

			lookupTable[i] = (int) intensity;
		}

		return lookupTable;
	}

	private static int[] getTargetCumulativeHistogram(byte[] bitmap, int originalPixelCount) {
		int[] cumulativeHistogram = HistogramUtils.createCumulativeHistogram(bitmap);

		int halfPixelCount = bitmap.length / 2;
		//
		// Match pixel count between reference and original images
		//
		for (int i = 0; i < cumulativeHistogram.length; i++) {
			//
			// Values may be big => long is preferred
			//
			long normalizedCount = (cumulativeHistogram[i] * ((long) originalPixelCount) + halfPixelCount) / bitmap.length;
			cumulativeHistogram[i] = (int) normalizedCount;
		}

		return cumulativeHistogram;
	}

	private static int[] createDiffLookUpTable(int[] lookupTable) {
		int lookupTableLength = lookupTable.length;
		int[] diffLookUpTable = new int[lookupTableLength];

		int i = 0;
		while (i < lookupTableLength - 1) {
			diffLookUpTable[i] = lookupTable[i + 1] - lookupTable[i];
			i = i + 1;
		}

		return diffLookUpTable;
	}

	private static int getOutputPixelValue(int lookUpValue, int noise) {
		return (255 * (lookUpValue + noise)) / (int) DEEP_WHITE_INTENSITY;
	}

	private static int[] createLookUpTableFromTiles(int posY, Canvas originalImage, Canvas referenceImage, byte[] originalTile, byte[] referenceTile) {
		int tileReferenceHeight = referenceImage.height / 2;
		int refPosY = (posY * referenceImage.height) / originalImage.height;
		if (refPosY > referenceImage.height - tileReferenceHeight) {
			//
			// Handling corner case where tile slides too far
			//
			refPosY = referenceImage.height - tileReferenceHeight;
		}

		originalImage.surfaceCopy(posY * originalImage.width, originalTile);
		int[] sourceCumulativeHistogram = HistogramUtils.createCumulativeHistogram(originalTile);

		referenceImage.surfaceCopy(refPosY * referenceImage.width, referenceTile);
		int[] targetCumulativeHistogram = getTargetCumulativeHistogram(referenceTile, originalTile.length);

		return createLookUpTable(sourceCumulativeHistogram, targetCumulativeHistogram);
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		Canvas originalImage = canvas.clone();
		originalImage.rgbToGray();
		referenceImage.rgbToGray();
		ProgressLog.loadingImage("reference image", referenceImage.width, referenceImage.height);
		ProgressLog.loadingImage("image to adjust", originalImage.width, originalImage.height);

		ProgressLog matchingProgess = new ProgressLog(originalImage.height / 2);

		byte[] originalTile = createTileFromCanvas(originalImage);
		byte[] referenceTile = createTileFromCanvas(referenceImage);

		int[] lookupTable = createLookUpTableFromTiles(0, originalImage, referenceImage, originalTile, referenceTile);
		int[] diffLookUpTable = createDiffLookUpTable(lookupTable);
		int firstQuarter = canvas.height / 4;
		for (int j = 0; j < firstQuarter; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma = originalImage.getRGB(i, j);
				int noise = DitherOperator.blueNoise(diffLookUpTable[luma], i, j);
				canvas.setRGB(i, j, getOutputPixelValue(lookupTable[luma], noise));
			}
		}

		int lastQuarter = (3 * canvas.height) / 4;
		for (int j = firstQuarter; j < lastQuarter; j++) {
			matchingProgess.display(j - firstQuarter);
			lookupTable = createLookUpTableFromTiles(j - firstQuarter, originalImage, referenceImage, originalTile, referenceTile);
			diffLookUpTable = createDiffLookUpTable(lookupTable);
			for (int i = 0; i < canvas.width; i++) {
				int luma = originalImage.getRGB(i, j);
				int noise = DitherOperator.blueNoise(diffLookUpTable[luma], i, j);
				canvas.setRGB(i, j, getOutputPixelValue(lookupTable[luma], noise));
			}
		}

		lookupTable = createLookUpTableFromTiles(lastQuarter - firstQuarter, originalImage, referenceImage, originalTile, referenceTile);
		diffLookUpTable = createDiffLookUpTable(lookupTable);
		for (int j = lastQuarter; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma = originalImage.getRGB(i, j);
				int noise = DitherOperator.blueNoise(diffLookUpTable[luma], i, j);
				canvas.setRGB(i, j, getOutputPixelValue(lookupTable[luma], noise));
			}
		}

		processorListener.exportCanvasMonochrom();
	}
}
