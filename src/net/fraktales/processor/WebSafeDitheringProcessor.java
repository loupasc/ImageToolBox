package net.fraktales.processor;

import net.fraktales.image.Canvas;
import net.fraktales.output.ProcessorListener;
import net.fraktales.param.DitherQuantization;
import net.fraktales.processor.util.KernelFilter;
import net.fraktales.processor.util.KernelFilterUtils;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;
import net.fraktales.tools.ProgressLog;

public class WebSafeDitheringProcessor extends AbstractProcessor {

	//
	// Progress log heuristic
	//
	private static final int ITERATION_RATIO = 3628800; // factorial(10)

	//
	// Numerical value corresponding to the maximum deep gray value
	//
	private static final int WHITE = ColorTransformation.rgbToDeepGray(255, 255, 255);

	//
	// Increment between two successive web safe color intensity
	//
	private static final int COLOR_SCALE = WHITE / (DitherQuantization.WEB_SAFE.colorCount - 1);

	//
	// Web safe grayscale palette
	//
	private static final int[] WEB_SAFE_PALETTE = createWebSafePalette();

	public WebSafeDitheringProcessor(ProcessorListener processorListener) {
		super(processorListener);
	}

	private static int getProgressionFromUpdatedPixelCount(int pixelUpdatedCount) {
		int progressIndex = 1;
		int threshold = ITERATION_RATIO;
		int thresholdDivisor = 10;
		while (pixelUpdatedCount < threshold) {
			threshold = threshold / thresholdDivisor;
			thresholdDivisor = thresholdDivisor - 1;
			progressIndex = progressIndex + 1;
		}

		return progressIndex;
	}

	private static int[] createWebSafePalette() {
		int paletteLength = DitherQuantization.WEB_SAFE.colorCount;
		int[] paletteArray = new int[paletteLength];

		for (int i = 0; i < paletteLength; i++) {
			paletteArray[i] = i * COLOR_SCALE;
		}

		return paletteArray;
	}

	private static boolean isWebSafe(int luma16bit) {
		for (int webSafeLuma : WEB_SAFE_PALETTE) {
			if (luma16bit == webSafeLuma) {
				return true;
			}
		}
		return false;
	}

	private static int tryToTogglePixel(Canvas image, int x, int y, int originalWebSafePixel, Canvas convolvedImage, KernelFilter kernel) {
		int originalConvolvedPixel = convolvedImage.getRGB(x, y);
		int diff = Math.abs(kernel.convolvePixel(x, y, image) - originalConvolvedPixel);
		// try
		int currentPixelValue = image.getRGB(x, y);
		if (currentPixelValue == originalWebSafePixel) {
			image.setRGB(x, y, currentPixelValue + COLOR_SCALE);
		}
		else {
			image.setRGB(x, y, originalWebSafePixel);
		}
		if (Math.abs(kernel.convolvePixel(x, y, image) - originalConvolvedPixel) < diff)  {
			return 1;
		}
		// else restore
		image.setRGB(x, y, currentPixelValue);
		return 0;
	}

	private static int tryToSwapPixel(Canvas image, int x, int y, Canvas convolvedImage, KernelFilter kernel) {
		int currentPixelValue = image.getRGB(x, y);
		int nextPixelValue = image.getRGB(x + 1, y);
		if (currentPixelValue != nextPixelValue) {
			int currentConvolvedPixel = convolvedImage.getRGB(x, y);
			int nextConvolvedPixel = convolvedImage.getRGB(x + 1, y);
			int currentDiff = Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel);
			int nextDiff = Math.abs(kernel.convolvePixel(x + 1, y, image) - nextConvolvedPixel);
			// try
			image.setRGB(x, y, nextPixelValue);
			image.setRGB(x + 1, y, currentPixelValue);
			if ((Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel) < currentDiff)
					&& (Math.abs(kernel.convolvePixel(x + 1, y, image) - nextConvolvedPixel) < nextDiff)) {
				return 1;
			}
			// else restore
			image.setRGB(x, y, currentPixelValue);
			image.setRGB(x + 1, y, nextPixelValue);
		}
		return 0;
	}

	private static int cleanPixel(Canvas image, int x, int y, Canvas convolvedImage, KernelFilter kernel) {
		int currentPixelValue = image.getRGB(x, y);
		int nextPixelValue = image.getRGB(x + 1, y);
		if (Math.abs(currentPixelValue - nextPixelValue) == 2 * COLOR_SCALE) {
			int currentConvolvedPixel = convolvedImage.getRGB(x, y);
			int nextConvolvedPixel = convolvedImage.getRGB(x + 1, y);
			int currentDiff = Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel);
			int nextDiff = Math.abs(kernel.convolvePixel(x + 1, y, image) - nextConvolvedPixel);
			// try
			int averagePixelValue = (currentPixelValue + nextPixelValue) / 2;
			image.setRGB(x, y, averagePixelValue);
			image.setRGB(x + 1, y, averagePixelValue);
			if (Math.abs(kernel.convolvePixel(x, y, image) - currentConvolvedPixel)
					+ Math.abs(kernel.convolvePixel(x + 1, y, image) - nextConvolvedPixel) < (currentDiff + nextDiff)) {
				return 1;
			}
			// else restore
			image.setRGB(x, y, currentPixelValue);
			image.setRGB(x + 1, y, nextPixelValue);
		}
		return 0;
	}

	private static Canvas createConvolvedImage(Canvas image) {
		Canvas convolvedImage = image.clone();

		for (int j = 1; j < (image.height - 1); j++) {
			for (int i = 1; i < (image.width - 1); i++) {
				KernelFilter kernel = KernelFilterUtils.getKernelFilterToToggle(i, j, image.width, image.height);
				convolvedImage.setRGB(i, j, kernel.convolvePixel(i, j, image));
			}
		}

		return convolvedImage;
	}

	private static int getWebSafe(int luma16bit, int ditherPattern) {
		return (11 * (5958 * ditherPattern + luma16bit)) >> 17;
	}

	@Override
	public void execute() {
		Canvas canvas = processorListener.getCanvas();
		ProgressLog.loadingImage("input image", canvas.width, canvas.height);
		//
		// convert to 16-bit luma
		//
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int pixelColor = canvas.getRGB(i, j);
				int luma16bit = ColorTransformation.rgbToDeepGray(BufferedImageUtils.getRed(pixelColor), 
						BufferedImageUtils.getGreen(pixelColor),
						BufferedImageUtils.getBlue(pixelColor));
				canvas.setRGB(i, j, luma16bit);
			}
		}

		ProgressLog ditheringProgress = new ProgressLog(10);

		Canvas sourceCanvas = canvas.clone();
		ditheringProgress.display(0);

		
		Canvas convolvedImage = createConvolvedImage(canvas);
		ditheringProgress.display(1);

		//
		// First step - Posterization of the image to process
		// Extreme border values are final and they are half-toned
		// using checker-board pattern.
		// Center values will be brighten so they are truncated.
		//
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma16bit = canvas.getRGB(i, j);
				if ((i == 0) || (i == canvas.width - 1) || (j == 0) || (j == canvas.height - 1)) {
					// Extreme border
					canvas.setRGB(i, j, COLOR_SCALE * getWebSafe(luma16bit, (i ^ j) & 0x1));
				}
				else {
					// Main area
					canvas.setRGB(i, j, COLOR_SCALE * (luma16bit / COLOR_SCALE));
				}
			}
		}

		int progressIndex = 2;
		ditheringProgress.display(progressIndex);

		//
		// First pass - Toggle pixels
		//
		int pixelUpdatedCount = Integer.MAX_VALUE;
		while (pixelUpdatedCount > 0) {
			// non-linear progression
			int newProgress = getProgressionFromUpdatedPixelCount(pixelUpdatedCount);
			while (progressIndex < newProgress) {
				progressIndex = progressIndex + 1;
				ditheringProgress.display(progressIndex);
			}
			// processing
			pixelUpdatedCount = 0;
			for (int j = 1; j < canvas.height - 1; j++) {
				for (int i = 1; i < canvas.width - 1; i++) {
					int originalPixel = sourceCanvas.getRGB(i, j);
					if (!isWebSafe(originalPixel)) {
						KernelFilter kernel = KernelFilterUtils.getKernelFilterToToggle(i, j, canvas.width, canvas.height);
						pixelUpdatedCount = pixelUpdatedCount
								+ tryToTogglePixel(canvas,
										i, j,
										COLOR_SCALE * (originalPixel / COLOR_SCALE),
										convolvedImage,
										kernel);
					}
				}
			}
		}

		while (progressIndex < 9) {
			progressIndex = progressIndex + 1;
			ditheringProgress.display(progressIndex);
		}

		//
		// Second pass - Swap pixels
		//
		pixelUpdatedCount = Integer.MAX_VALUE;
		while (pixelUpdatedCount > 0) {
			pixelUpdatedCount = 0;
			for (int j = 1; j < canvas.height - 1; j++) {
				for (int i = 1; i < canvas.width - 2; i++) {
					if ((!isWebSafe(sourceCanvas.getRGB(i, j))) && (!isWebSafe(sourceCanvas.getRGB(i + 1, j)))) {
						KernelFilter kernel = KernelFilterUtils.getKernelFilterToSwap(i, j, canvas.width, canvas.height);
						pixelUpdatedCount = pixelUpdatedCount
								+ tryToSwapPixel(canvas, i, j,
										convolvedImage,
										kernel);
					}
				}
			}
		}

		//
		// Replace dirty pixels (2 successive pixels with an difference 2 COLOR_SCALE intensity)
		// with the intermediate intensity
		//
		for (int j = 1; j < canvas.height - 1; j++) {
			for (int i = 1; i < canvas.width - 2; i++) {
				if ((!isWebSafe(sourceCanvas.getRGB(i, j))) && (!isWebSafe(sourceCanvas.getRGB(i + 1, j)))) {
					KernelFilter kernel = KernelFilterUtils.getKernelFilterToSwap(i, j, canvas.width, canvas.height);
					cleanPixel(canvas, i, j, convolvedImage, kernel);
				}
			}
		}

		ditheringProgress.display(10);

		//
		// convert to 8-bit RGB
		//
		for (int j = 0; j < canvas.height; j++) {
			for (int i = 0; i < canvas.width; i++) {
				int luma8bit = (canvas.getRGB(i, j) * 255) / WHITE;
				canvas.setRGB(i, j, BufferedImageUtils.packRgbBytes(luma8bit, luma8bit, luma8bit));
			}
		}

		processorListener.exportCanvasTrueColor();
	}
}
