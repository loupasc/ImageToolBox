package net.fraktales.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.RenderedImage;

import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ColorTransformation;

public class Canvas implements Cloneable {

	public static enum Channel {
		BLUE(0),
		GREEN(8),
		RED(16);

		public final int bandShift;

		private Channel(int bandShift) {
			this.bandShift = bandShift;
		}
	}

	public final int width;
	public final int height;
	private final int[] surface;
	private final RenderedImage image;

	private Canvas(int[] surface, int width, int height, RenderedImage image, int type) {
		if (type != BufferedImage.TYPE_INT_RGB) throw new IllegalArgumentException("Image type shall be TYPE_INT_RGB");
		this.surface = surface;
		this.width = width;
		this.height = height;
		this.image = image;
	}

	public Canvas(BufferedImage image) {
		this(((DataBufferInt) image.getRaster().getDataBuffer()).getData(), image.getWidth(), image.getHeight(), image, image.getType());
	}

	public Canvas(int width, int height) {
		this(new int[width * height], width, height, null, BufferedImage.TYPE_INT_RGB);
	}

	private static int surfaceExtend(int x, int maxValue) {
		if (x < 0) {
			return 0;
		}
		if (x < maxValue) {
			return x;
		}
		return (maxValue - 1);
	}

	public int getRGB(int x, int y) {
		return surface[y * width + x];
	}

	public void setRGB(int x, int y, int rgb) {
		surface[y * width + x] = rgb;
	}

	public int safeGetRGB(int x, int y) {
		if ((x >= 0) && (x < width) && (y >=0) && (y < height)) {
			return surface[y * width + x];
		}
		return surface[surfaceExtend(y, height) * width + surfaceExtend(x, width)];
	}

	public void safeSetRGB(int x, int y, int rgb) {
		if ((x >= 0) && (x < width) && (y >=0) && (y < height)) {
			surface[y * width + x] = rgb;
		}
	}

	public void rgbToGray() {
		int offset = 0;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				surface[offset] = ColorTransformation.rgbToGray(BufferedImageUtils.getRed(surface[offset]),
						BufferedImageUtils.getGreen(surface[offset]),
						BufferedImageUtils.getBlue(surface[offset]),
						(i ^ j) & 0x1);
				offset = offset + 1;
			}
		}
	}

	public void surfaceCopy(Channel channel, int srcPos, byte[] dest) {
		for (int i = 0; i < dest.length; i++) {
			dest[i] = (byte) (surface[srcPos + i] >> channel.bandShift);
		}
	}

	public void surfaceCopy(int srcPos, byte[] dest) {
		surfaceCopy(Channel.BLUE, srcPos, dest);
	}

	public RenderedImage getRenderedImage() {
		return image;
	}

	@Override
	public Canvas clone() {
		return new Canvas(surface.clone(), width, height, null, BufferedImage.TYPE_INT_RGB);
	}
}
