package net.fraktales.output;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.fraktales.image.Canvas;
import net.fraktales.tools.BufferedImageUtils;
import net.fraktales.tools.ConsoleLogger;

public class ImageFileWriter implements ProcessorListener {

	private final Canvas canvas;
	private final String imageType;
	private final File outputFile;

	public ImageFileWriter(Canvas canvas, String filename) {
		this.canvas = canvas;
		imageType = filename.substring(filename.indexOf('.') + 1);
		outputFile = new File(filename);
	}

	@Override
	public Canvas getCanvas() {
		return canvas;
	}

	private void exportCanvas(RenderedImage imageToExport) {
		try {
			ImageIO.write(imageToExport, imageType, outputFile);
			ConsoleLogger.getInstance().info("Done - Create " + outputFile);
		}
		catch (IOException e) {
			ConsoleLogger.getInstance().error("Cannot create output image file - " + e.getMessage());
		}
	}

	@Override
	public void exportCanvasIndexedColor() {
		int halfWidth = canvas.width / 2;
		BufferedImage indexedColorImage = BufferedImageUtils.createIndexedBufferedImage(canvas.width, canvas.height);
		byte[] bitmap = ((DataBufferByte) indexedColorImage.getRaster().getDataBuffer()).getData();
		for (int j = 0; j < canvas.height; j++) {
			int posY = canvas.width * j;
			for (int i = 0; i < halfWidth; i++) {
				int pixelColor0 = canvas.getRGB(2 * i, j);
				int pixelColor1 = canvas.getRGB(2 * i + 1, j);
				bitmap[posY + i] = (byte) ((BufferedImageUtils.getBinaryRed(pixelColor0)
						| BufferedImageUtils.getBinaryGreen(pixelColor0)
						| BufferedImageUtils.getBinaryBlue(pixelColor0)) << 4
						| BufferedImageUtils.getBinaryRed(pixelColor1)
						| BufferedImageUtils.getBinaryGreen(pixelColor1)
						| BufferedImageUtils.getBinaryBlue(pixelColor1));
			}
		}
		exportCanvas(indexedColorImage);
	}

	@Override
	public void exportCanvasMonochrom() {
		BufferedImage monochromImage = new BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_BYTE_GRAY);
		byte[] bitmap = ((DataBufferByte) monochromImage.getRaster().getDataBuffer()).getData();
		canvas.surfaceCopy(0, bitmap);
		exportCanvas(monochromImage);
	}

	@Override
	public void exportCanvasTrueColor() {
		exportCanvas(canvas.getRenderedImage());
	}
}
