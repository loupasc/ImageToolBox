package net.fraktales.output;

import net.fraktales.image.Canvas;

public interface ProcessorListener {
	public Canvas getCanvas();
	public void exportCanvasIndexedColor();
	public void exportCanvasMonochrom();
	public void exportCanvasTrueColor();
}
